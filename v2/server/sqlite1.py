import sqlite3, json
#import pymysql
import sys
import pprint

'''
	get last error msg
'''
def GetLastErrorMsg():
	return sys.exc_info()[1]
	
# db class antrian
class DBAntrian(object):
	#def __init__(self, driver='mysql', hostname='localhost', username='admin_db', password='1234', dbname='raspi-fish-feeder'):
	def __init__(self, driver='sqlite', hostname=None, username=None, password=None, dbname='antrian.db'):
		try:
			self.type = driver
			self.dbname = dbname
			
			if self.type == 'sqlite':
				self.con = sqlite3.connect(self.dbname)
				
			if self.type == 'mysql':
				self.con = pymysql.connect(hostname,username,password,dbname)
			
			self.init_success = True
		except:
			pass
			self.init_success = False
		   
	def close(self):
		try:
			#if self.type == 'sqlite':
			self.con.close()
				
			#if self.type == 'mysql':
			#	self.con.close()
		except:
			pass
		   
	'''
	def get_schedule_list(self, in_json = False):
		try:			
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute('SELECT * FROM feed_schedule ORDER BY id')
				rows = cur.fetchall()
					
				if in_json:
					return json.dumps([dict(ix) for ix in rows])
				return [dict(ix) for ix in rows]
			return None
		except:
			pass
			return None		
	#https://stackoverflow.com/questions/21986194/how-to-pass-dictionary-items-as-function-arguments-in-python
	def update_schedule(self, id=None, aktif=0, senin=0, selasa=0, rabu=0, kamis=0, jumat=0, sabtu=0, minggu=0, jam='00:00:00', berat=0.0):
		try:
			if self.init_success and (id is not None):
				cur = self.con.cursor()
				cur.execute('update feed_schedule set aktif=?, senin=?, selasa=?, rabu=?, kamis=?, jumat=?, sabtu=?, minggu=?, jam=?, berat=? where id=?',(aktif, senin, selasa, rabu, kamis, jumat, sabtu, minggu, jam, berat, id))
				self.con.commit()
				return cur.rowcount
			return 0
		except:
			pass
			return 0
	'''
			
	# get new line up number
	def get_queue_number(self, pool_num=None):
		try:
			new_number = -1
			if self.init_success and (pool_num is not None):
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				# check for pool data
				cur.execute("select * from master_nomor where pool=?", (str(pool_num)))
				rows = cur.fetchall()
				#print(rows)
				
				# got data
				if len(rows) != 0:
					# got pool data, check the date
					cur.execute("select * from master_nomor where pool=? and date(waktu,'localtime')=date('now','localtime') order by waktu desc limit 1", (str(pool_num)))
					rows = cur.fetchall()
					#print(rows)
					#print(json.dumps([dict(ix) for ix in rows]))
					
					#already have the data, increase the number
					if len(rows) !=0:
						#old_number = int(rows[0]['nomor'])
						new_number = int(rows[0]['nomor']) + 1
						
						#update items
						cur.execute("update master_nomor set nomor=?, waktu=datetime('now','localtime') where pool=?", (new_number,pool_num))
						self.con.commit()
						
						#print(new_number)
					
					#init the data for today
					else:
						cur.execute("insert or replace into master_nomor(pool,nomor) values(?,1)", str(pool_num))
						self.con.commit()
						new_number = 1
					
					
				# no data, insert new one
				else:
					cur.execute("insert into master_nomor(pool,nomor) values(?,1)", (str(pool_num)))
					self.con.commit()
					new_number = 1
					
			#return new_number
				
		except:
			pass
			print(GetLastErrorMsg())
			new_number = -1
			
		return new_number
		
	'''
	get all queue list
	'''
	def get_queue_list(self, in_json = False, in_csv = False):
		try:			
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute("""
					SELECT a.*, ifnull(b.nomor,0) nomor, ifnull(b.waktu,datetime('now','localtime')) waktu, ifnull(b.status,0) status
					FROM master_cso a
					LEFT JOIN log_cso b
					ON a.id_cso = b.id_cso AND date(b.waktu,'localtime')=date('now','localtime')
					ORDER BY a.id_cso ASC
				""")
				rows = cur.fetchall()
				
				# as csv
				if in_csv:
					resx = ""
					for ix in rows:
						resx += str(ix['nomor'])+","
					return resx[:-1]
					
				# as json
				if in_json:
					return json.dumps([dict(ix) for ix in rows])
					
				# as dictionary
				return [dict(ix) for ix in rows]
			return None
		except:
			pass
			return None

	'''
	get all queue need announcement
	'''
	def get_queue_announce_list(self):
		try:
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute("""
					select no_urut,id_cso,nomor from log_cso where date(waktu,'localtime')=date('now','localtime') and status=0 order by id_cso
				""")
				rows = cur.fetchall()
				# as dictionary
				return [dict(ix) for ix in rows]
			
			return None
		except:
			pass
			return None
			
	'''
	update status nomor cso
	'''
	def update_cso_queue_status(self,new_status=None,data_key=None):
		try:
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute("""
					update log_cso
					set status=?
					where no_urut=?
				""", (new_status, data_key))
				self.con.commit()
				
				#print(cur.rowcount)
				
				# as dictionary
				return cur.rowcount==1
			
			return False
		except:
			pass
			return False
			
'''
Main program start here
'''			
if __name__ == "__main__":
	dbx = DBAntrian()
	#print(dbx.get_queue_number(pool_num=1))
	pprint.pprint(dbx.get_queue_list(in_csv=True))
	pprint.pprint(dbx.get_queue_announce_list())
	pprint.pprint(dbx.update_cso_queue_status(data_key=1,new_status=0))
	dbx.close()



