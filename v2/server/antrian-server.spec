# -*- mode: python -*-

block_cipher = None


a = Analysis(['antrian-server.py'],
             pathex=['/mnt/data/antrian/server'],
             binaries=[],
             datas=[],
             hiddenimports=['six', 'appdirs', 'packaging', 'packaging', 'packaging.version', 'packaging.specifiers', 'packaging.requirements'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='antrian-server',
          debug=False,
          strip=True,
          upx=True,
          console=True )
