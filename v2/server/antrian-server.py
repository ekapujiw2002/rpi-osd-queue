'''
server antrian raspi 
versi : 18-09-2018
https://gist.github.com/micktwomey/606178
compile to one executable using :
python2 -m PyInstaller --strip --onefile --clean --hidden-import=six --hidden-import=appdirs --hidden-import=packaging --hidden-import=packaging --hidden-import=packaging.version --hidden-import=packaging.specifiers --hidden-import=packaging.requirements
'''

import subprocess, os
import sys
import multiprocessing
import socket
import time, datetime
from string import Formatter

#db
import sqlite3, json

#gpio
import RPi.GPIO as GPIO

#printer
from escpos import constants, escpos, exceptions, printer
from escpos.printer import File
#import escpos

#logging
import logging

#basic configuration logging
logging.basicConfig(level=logging.DEBUG)

'''
info beamer port udp
'''
INFO_BEAMER_UDP = {
    'HOST': '127.0.0.1',
    'PORT': 4444
}
INFO_BEAMER_SOCKET_ADDR = (INFO_BEAMER_UDP['HOST'],INFO_BEAMER_UDP['PORT'])
# info beamer udp socket
INFO_BEAMER_SOCKET = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

# global vars
APP_VERSION = "18092018.141745"
SOUND_DIRECTORY = "/data/project/info-beamer/tes/antrian-old/src/server/snd/"
PRINTER_OUTPUT = None
ANTRIAN_DB_OBJ = None
GPIO_OBJ = None

'''
    get last error msg
'''
def GetLastErrorMsg():
    return sys.exc_info()[1]
    
'''
Info beamer udp command
'''
def InfoBeamerUDPCommand(cmdx=None):
    err_val=0
    err_msg=''
    try:
        if cmdx is not None:
            sent = INFO_BEAMER_SOCKET.sendto(cmdx.encode(),INFO_BEAMER_SOCKET_ADDR)
            if sent == len(cmdx):
                err_msg = 'OK'
                err_val = 0
            else:
                err_msg = 'Not all message sent'
                err_val = 1
        else:
            err_msg = 'No command'
            err_val = 1
    except:
        pass
        err_msg = GetLastErrorMsg()
        err_val = 1
        
    return (err_val,err_msg)

'''
https://stackoverflow.com/a/42320260
https://stackoverflow.com/questions/538666/python-format-timedelta-to-string
'''
def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02}s', inputtype='timedelta'):
    """Convert a datetime.timedelta object or a regular number to a custom-
    formatted string, just like the stftime() method does for datetime.datetime
    objects.

    The fmt argument allows custom formatting to be specified.  Fields can 
    include seconds, minutes, hours, days, and weeks.  Each field is optional.

    Some examples:
        '{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
        '{W}w {D}d {H}:{M:02}:{S:02}'     --> '4w 5d 8:04:02'
        '{D:2}d {H:2}:{M:02}:{S:02}'      --> ' 5d  8:04:02'
        '{H}h {S}s'                       --> '72h 800s'

    The inputtype argument allows tdelta to be a regular number instead of the  
    default, which is a datetime.timedelta object.  Valid inputtype strings: 
        's', 'seconds', 
        'm', 'minutes', 
        'h', 'hours', 
        'd', 'days', 
        'w', 'weeks'
    """

    # Convert tdelta to integer seconds.
    if inputtype == 'timedelta':
        remainder = int(tdelta.total_seconds())
    elif inputtype in ['s', 'seconds']:
        remainder = int(tdelta)
    elif inputtype in ['m', 'minutes']:
        remainder = int(tdelta)*60
    elif inputtype in ['h', 'hours']:
        remainder = int(tdelta)*3600
    elif inputtype in ['d', 'days']:
        remainder = int(tdelta)*86400
    elif inputtype in ['w', 'weeks']:
        remainder = int(tdelta)*604800

    f = Formatter()
    desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
    possible_fields = ('W', 'D', 'H', 'M', 'S')
    constants = {'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
    values = {}
    for field in possible_fields:
        if field in desired_fields and field in constants:
            values[field], remainder = divmod(remainder, constants[field])
    return f.format(fmt, **values)
    
'''
read end file
'''
def read_end_datetime_file():
    try:
        try:
            file = open("/mnt/data/endtime.txt","r")
        except:
            pass
            
        try:
            file = open("/etc/countdown/endtime.txt","r")
        except:
            pass
            
        try:
            file = open("endtime.txt","r")
        except:
            pass
            
        strx = file.read()
        file.close()
        strx = strx.rstrip("\r\n")
        return strx
    except:
        pass
        return "2018-8-18 0:0:0"

'''
get day diff time
'''
def get_delta_days_time():
    try:
        d0 = datetime.datetime.strptime(read_end_datetime_file(),"%Y-%m-%d %H:%M:%S")
        d1 = datetime.datetime.now()
        return strfdelta(d0-d1,"{D},{H},{M},{S}")
    except:
        pass
        return "0,0,0,0"

'''
main connection handler
'''
def server_connection_handler(connection, address):
    import logging
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger("process-%r" % (address,))
    try:
        logger.debug("Connected %r at %r", connection, address)
        while True:
            '''
            data = connection.recv(1024)
            if data == "":
                logger.debug("Socket closed remotely")
                break
            logger.debug("Received data %r", data)
            '''
            '''
            connection.sendall(data)
            connection.sendall(get_delta_days_time())
            logger.debug("Sent data")
            '''
            
            data = connection.recv(1024)
            if data == b"":
                logger.debug("Socket closed remotely")
                break
            logger.debug("Received data %r", data)
            
            try:
                if len(data)>0:
                    #split the command
                    cmd_detil = data.decode().split(',')
                    logger.debug("Parsed command = %r", cmd_detil)
                    
                    cmd_head = cmd_detil[0]
                    cmd_id_cso = int(cmd_detil[1])
                                    
                    #command request nomor 
                    #send data to display and make announcement
                    if cmd_head == 'c':
                        logger.debug("Request new number for LINE %s", cmd_detil[1])
                        new_number_line = ANTRIAN_DB_OBJ.get_line_number_allocation(cso_number=cmd_id_cso)
                        logger.debug("Got new number allocate = %d", new_number_line)
                        if(sound_announcement(id_cso=cmd_id_cso, number=new_number_line)):
                            logger.debug("Updating status = %s", ANTRIAN_DB_OBJ.update_cso_calling_status(id_cso=cmd_id_cso,new_status=1))
                            
                        #cmdx_display = "osd/count:%s" % (ANTRIAN_DB_OBJ.get_queue_list(in_csv=True))
                        #logger.debug("Send data display = %s", cmdx_display)
                        #InfoBeamerUDPCommand(cmdx=)                    
                        
                    #command recall
                    if cmd_head == 'r':
                        logger.debug("Recall for LINE %s", cmd_detil[1])
                        logger.debug("Result = %s", ANTRIAN_DB_OBJ.update_cso_calling_status(id_cso=cmd_id_cso,new_status=0))
                        new_number_line = ANTRIAN_DB_OBJ.get_line_not_announced_number(cso_number=cmd_id_cso)
                        if(new_number_line>0):
                            if(sound_announcement(id_cso=cmd_id_cso, number=new_number_line)):
                                logger.debug("Updating status = %s", ANTRIAN_DB_OBJ.update_cso_calling_status(id_cso=cmd_id_cso,new_status=1))
                        #sound_announcement(id_cso=cmd_id_cso, number=new_number_line)
                    
                    #command reinit db
                    if cmd_head == 'x' and cmd_id_cso == 255:
                        logger.debug("Reinit database")
                        logger.debug("Result = %s", ANTRIAN_DB_OBJ.reinit_database_content())
                    
            except:
                pass
                logger.exception("Got error handling the command")
            #time.sleep(0.05)
    except:
        logger.exception("Problem handling request")
    finally:
        logger.debug("Closing socket")
        connection.close()

'''
main server class for clients
'''
class Server(object):
    '''
    init the server
    '''
    def __init__(self, hostname, port):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("server")
        self.hostname = hostname
        self.port = port

    '''
    start the server as daemon
    https://stackoverflow.com/questions/29217502/socket-error-address-already-in-use
    '''
    def start(self):
        self.logger.debug("listening")
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.hostname, self.port))
        self.socket.listen(1)

        while True:
            conn, address = self.socket.accept()
            self.logger.debug("Got connection")
            process = multiprocessing.Process(target=server_connection_handler, args=(conn, address))
            process.daemon = True
            process.start()
            self.logger.debug("Started process %r", process)
        
'''
gpio handler class
'''        
class GPIO_CLS(object):
    def __init__(self, printer_output=None, db_connection=None):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("gpio")
        self.is_printing = False
            
        try:
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.add_event_detect(26, GPIO.FALLING, callback=self.gpio_switch_callback, bouncetime=200)
            self.printer_output = printer_output
            self.db_connection = db_connection
            
            self.logger.debug("GPIO init OK")
        except:
            pass
            self.logger.exception("GPIO init fail at %s" % (GetLastErrorMsg()))
        
    '''
    cleanup
    '''
    def cleaning(self):
        try:
            GPIO.cleanup()
            self.logger.debug("GPIO cleanup OK")
        except:
            pass
            self.logger.exception("GPIO cleanup fail at %s" % (GetLastErrorMsg()))
    
    '''
    when user press button
    '''
    def gpio_switch_callback(self, channel):
        try:
            if not self.is_printing:
                self.is_printing = True
                self.logger.debug("GPIO start printing...")
                
                print("%f : GPIO %s pressed" % (time.time(), channel))
                new_queue_number = self.db_connection.get_queue_number(pool_num=1)
                if new_queue_number != -1:
                    self.printer_output.print_note(numberx=channel)
                    self.logger.debug("GPIO printing OK")
                else:
                    self.logger.exception("GPIO printing fail at requesting new queue number")                
                
                self.is_printing = False
        except:
            pass
            self.is_printing = False
            self.logger.exception("GPIO printing fail at %s" % (GetLastErrorMsg()))
        
'''
printing struk class
'''
class PRINTER_EPSON(object):
    def __init__(self,printer_dev_name="/dev/usb/lp0"):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("printer")
        
        try:
            self.printer_obj = printer.File(printer_dev_name)
            self.logger.debug("Printer init OK")
        except:
            pass
            self.printer_obj = None
            self.logger.exception("Printer init fail at %s" % (GetLastErrorMsg()))
                    
    def print_note(self,numberx=0):
        try:
            self.logger.debug("Printer start printing")
            
            if self.printer_obj is not None:
                try:
                    self.printer_obj.set(align='center', text_type='b', height=2, width=2)
                except:
                    pass
                    self.logger.exception("Printer set custom text option fail at %s" % (GetLastErrorMsg()))
                    
                self.printer_obj.textln("\nSELAMAT DATANG\nDISNAKER KOTA SUKABUMI\n")
                
                try:
                    self.printer_obj.set(align='center', text_type='b',height=6, width=3)
                except:
                    pass
                    self.logger.exception("Printer set custom text option fail at %s" % (GetLastErrorMsg()))
                    
                self.printer_obj.textln(str(numberx))
                
                try:
                    self.printer_obj.set(align='center', text_type='normal',height=2, width=2)
                except:
                    pass
                    self.logger.exception("Printer set custom text option fail at %s" % (GetLastErrorMsg()))
                    
                self.printer_obj.textln(str(datetime.datetime.strftime(datetime.datetime.now(), "%d/%m/%Y %H:%M:%S")))
                self.printer_obj.textln("Terimakasih Anda telah tertib\n")
                
                self.printer_obj.cut()
            else:
                self.logger.warning("Printer not opened!!!")
            
            self.logger.debug("Printer end printing")
            
            return True
        except:
            pass
            self.logger.exception("Printer note fail at %s" % (GetLastErrorMsg()))
            return False
            
# db class antrian
# https://stackoverflow.com/questions/393554/python-sqlite3-and-concurrency
class DB_ANTRIAN(object):
    #def __init__(self, driver='mysql', hostname='localhost', username='admin_db', password='1234', dbname='raspi-fish-feeder'):
    def __init__(self, driver='sqlite', hostname=None, username=None, password=None, dbname='antrian.db'):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        logging.basicConfig()
        
        self.logger = logging.getLogger("db")
        self.logger.debug("Initializing...")
        
        try:
            self.type = driver
            self.dbname = dbname
            
            if self.type == 'sqlite':
                # vacuum first
                
                self.con = sqlite3.connect(self.dbname, check_same_thread = False)
                
            if self.type == 'mysql':
                self.con = pymysql.connect(hostname,username,password,dbname)
            
            self.init_success = True
        except:
            pass
            self.init_success = False
            
        self.logger.debug("Init result %s" % (self.init_success))
           
    def close(self):
        try:
            self.logger.debug("Close database OK")
            
            #if self.type == 'sqlite':
            self.con.close()
                
            #if self.type == 'mysql':
            #    self.con.close()
        except:
            pass
            self.logger.exception("Close database fail at %s" % (GetLastErrorMsg()))
            
    def reinit_database_content(self):
        try:
            if self.init_success:
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                
                # check for pool data
                cur.executescript("""
                DELETE FROM log_cso;
                DELETE FROm master_nomor;
                """)
                
                self.logger.debug("Reinit database OK")
        except:
            pass
            self.logger.exception("Reinit database fail at %s" % (GetLastErrorMsg()))
           
    '''
    def get_schedule_list(self, in_json = False):
        try:            
            if self.init_success:
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                
                cur.execute('SELECT * FROM feed_schedule ORDER BY id')
                rows = cur.fetchall()
                    
                if in_json:
                    return json.dumps([dict(ix) for ix in rows])
                return [dict(ix) for ix in rows]
            return None
        except:
            pass
            return None        
    #https://stackoverflow.com/questions/21986194/how-to-pass-dictionary-items-as-function-arguments-in-python
    def update_schedule(self, id=None, aktif=0, senin=0, selasa=0, rabu=0, kamis=0, jumat=0, sabtu=0, minggu=0, jam='00:00:00', berat=0.0):
        try:
            if self.init_success and (id is not None):
                cur = self.con.cursor()
                cur.execute('update feed_schedule set aktif=?, senin=?, selasa=?, rabu=?, kamis=?, jumat=?, sabtu=?, minggu=?, jam=?, berat=? where id=?',(aktif, senin, selasa, rabu, kamis, jumat, sabtu, minggu, jam, berat, id))
                self.con.commit()
                return cur.rowcount
            return 0
        except:
            pass
            return 0
    '''
            
    '''
    get new line up number
    run when button clicked
    result printed to paper
    '''
    def get_queue_number(self, pool_num=None):
        try:
            new_number = -1
            if self.init_success and (pool_num is not None):
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                
                # check for pool data
                cur.execute("select * from master_nomor where pool=? limit 1", (str(pool_num)))
                rows = cur.fetchall()
                #print(rows)
                
                # got data
                if len(rows) != 0:
                    # got pool data, check the date
                    cur.execute("select * from master_nomor where pool=? and date(waktu)=date('now','localtime') order by waktu desc limit 1", (str(pool_num)))
                    rows = cur.fetchall()
                    #print(rows)
                    #print(json.dumps([dict(ix) for ix in rows]))
                    
                    #already have the data, increase the number
                    if len(rows) !=0:
                        #old_number = int(rows[0]['nomor'])
                        new_number = int(rows[0]['nomor']) + 1
                        
                        #update items
                        cur.execute("update master_nomor set nomor=?, waktu=datetime('now','localtime') where pool=?", (new_number,pool_num))
                        self.con.commit()
                        
                        #print(new_number)
                    
                    #init the data for today
                    else:
                        cur.execute("insert or replace into master_nomor(pool,nomor) values(?,1)", str(pool_num))
                        self.con.commit()
                        new_number = 1
                    
                    
                # no data, insert new one
                else:
                    cur.execute("insert into master_nomor(pool,nomor) values(?,1)", (str(pool_num)))
                    self.con.commit()
                    new_number = 1
                    
            #return new_number
            self.logger.debug("Request new number OK with result %d" % (new_number))
                
        except:
            pass
            #print(GetLastErrorMsg())
            self.logger.exception("Request new number fail at %s" % (GetLastErrorMsg()))
            new_number = -1
            
        return new_number
        
    '''
    get all queue list for any cso
    '''
    def get_queue_list(self, in_json = False, in_csv = False):
        try:            
            if self.init_success:
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                
                cur.execute("""
                    SELECT a.*, ifnull(b.nomor,0) nomor, ifnull(b.waktu,datetime('now','localtime')) waktu, ifnull(b.status,0) status
                    FROM master_cso a
                    LEFT JOIN log_cso b
                    ON a.id_cso = b.id_cso AND date(b.waktu)=date('now','localtime')
                    GROUP BY a.id_cso
                    ORDER BY a.id_cso ASC, b.waktu DESC
                """)
                rows = cur.fetchall()
                
                # as csv
                if in_csv:
                    resx = ""
                    for ix in rows:
                        resx += str(ix['nomor'])+","
                    return resx[:-1]
                    
                # as json
                if in_json:
                    return json.dumps([dict(ix) for ix in rows])
                    
                # as dictionary
                return [dict(ix) for ix in rows]
            return None
        except:
            pass
            return None

    '''
    get all queue need announcement
    '''
    def get_queue_announce_list(self):
        try:
            if self.init_success:
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                    
                cur.execute("""
                    select id_cso,nomor from log_cso where date(waktu)=date('now','localtime') and status=0 order by waktu
                """)
                rows = cur.fetchall()
                # as dictionary
                return [dict(ix) for ix in rows]
            
            return None
        except:
            pass
            return None
            
    '''
    update status nomor cso
    '''
    def update_cso_queue_status(self,new_status=None,data_key=None):
        try:
            if self.init_success:
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                
                cur.execute("""
                    update log_cso
                    set status=?
                    where no_urut=?
                """, (str(new_status), str(data_key)))
                self.con.commit()
                
                #print(cur.rowcount)
                
                # as dictionary
                return cur.rowcount==1
            
            return False
        except:
            pass
            return False
            
    '''
    allocate new number from master_cso to log_cso for a line
    this is do calling
    '''
    def get_line_number_allocation(self, cso_number=None):
        try:
            new_number = -1
            if self.init_success:
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                    
                # cso is registered?
                cur.execute('''
                    SELECT pool
                    FROM master_cso
                    WHERE id_cso=?
                ''', (str(cso_number)))
                rows = cur.fetchone()
                if rows is None:
                    return -5
                lane_number = int(rows[0])
                
                # got any client for this pool lane number for today?
                cur.execute('''
                    SELECT nomor
                    FROM master_nomor
                    WHERE pool=? AND date(waktu)=date('now','localtime')
                ''', (str(lane_number)))
                rows = cur.fetchone()
                
                # no client for this pool for today
                # set default to 0
                if rows is None:
                    return -2
                    
                # request client not yet assigned
                no_max = int(rows[0])
                #print(no_max)
                cur.execute(
                '''
                    SELECT * 
                    FROM
                    (
                    SELECT ifnull(max(nomor),0) nomor_max
                    FROM log_cso
                    WHERE date(waktu)=date('now','localtime') AND (pool=?)
                    ) a
                    WHERE a.nomor_max<?
                ''', (int(lane_number), no_max)
                )
                rows = cur.fetchone()
                #print(rows)                
                #pprint.pprint(rows[0])
                
                # all client already assigned
                if rows is None:
                    return -3
                    
                #assigned the next in the queue
                no_max = int(rows[0]) + 1
                #print(no_max)
                
                #insert into log_cso
                cur.execute(
                '''
                    INSERT OR REPLACE INTO log_cso(id_cso,pool,nomor) values(?,?,?)
                ''', (int(cso_number),int(lane_number),no_max)
                )
                self.con.commit()
                
                #print(cur.rowcount)
                
                # as dictionary
                if cur.rowcount==1:
                    return no_max
                else:
                    return -4
            
        except:
            pass
            print(GetLastErrorMsg())
            new_number = -1
        
        return new_number
        
    '''
    force status to called(1) or no call (0)
    '''
    def update_cso_calling_status(self,id_cso=None,new_status=None):
        try:
            if self.init_success:
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                
                cur.execute("""
                    UPDATE log_cso
                    SET status=?
                    WHERE id_cso=? AND waktu=(
                    SELECT waktu
                    FROM log_cso
                    WHERE id_cso=?
                    ORDER BY waktu DESC
                    LIMIT 1
                    )
                """, (str(new_status), str(id_cso), str(id_cso)))
                self.con.commit()
                
                #print(cur.rowcount)
                
                # as dictionary
                return cur.rowcount==1
            
            return False
        except:
            pass
            return False
            
    '''
    get not announced number for a line
    '''
    def get_line_not_announced_number(self, cso_number=None):
        try:
            new_number = -1
            if self.init_success:
                if self.type == 'sqlite':
                    self.con.row_factory = sqlite3.Row
                    cur = self.con.cursor()                    
                    
                if self.type == 'mysql':
                    #https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
                    cur = self.con.cursor(pymysql.cursors.DictCursor)
                    
                # cso is regietered?
                cur.execute('''
                    SELECT nomor
                    FROM log_cso
                    WHERE id_cso=? and (date(waktu)=date('now','localtime')) and (status=0)
                    ORDER BY waktu DESC
                    LIMIT 1
                ''', (str(cso_number)))
                rows = cur.fetchone()
                if rows is None:
                    return -3
                new_number = int(rows[0])
                #return new_number
                
            return new_number
                    
        except:
            pass
            self.logger.exception(GetLastErrorMsg())
            return -1
    
'''
spawn new process
'''
def spawn_new_multiprocess_action(targetx=None):
    try:
        processx = multiprocessing.Process(target=targetx)
        processx.daemon = True
        processx.start()
        return processx
    except:
        pass
        return None
        
'''
make number to sound array
'''        
def number_to_sound_array(number=None):
    try:
        if number is not None:
            snd_str = ""
            
            # decode ribuan
            cnt_num = int(number)//1000
            if(cnt_num>0):
                if(cnt_num==1):
                    snd_str += " " + SOUND_DIRECTORY + "1000.wav"
                else:
                    snd_str += " " + SOUND_DIRECTORY + str(cnt_num) + ".wav " + SOUND_DIRECTORY + "ribu.wav"
                    
            # decode ratusan
            cnt_num = (int(number)%1000)//100
            if(cnt_num>0):
                if(cnt_num==1):
                    snd_str += " " + SOUND_DIRECTORY + "100.wav"
                else:
                    snd_str += " " + SOUND_DIRECTORY + str(cnt_num) + ".wav " + SOUND_DIRECTORY + "ratus.wav"
                    
            # decode puluhan
            cnt_num_plh = int(number)%100
            cnt_num = cnt_num_plh//10
            if(cnt_num>0):
                # sepuluh
                if(cnt_num_plh==10):
                    snd_str += " " + SOUND_DIRECTORY + "10.wav"
                # sebelas
                elif(cnt_num_plh==11):
                    snd_str += " " + SOUND_DIRECTORY + "11.wav"
                # 11<x<20
                elif(11<cnt_num_plh<20):
                    snd_str += " " + SOUND_DIRECTORY + str(cnt_num_plh%10) + ".wav " + SOUND_DIRECTORY + "belas.wav"
                # >=20
                else:
                    snd_str += " " + SOUND_DIRECTORY + str(cnt_num) + ".wav " + SOUND_DIRECTORY + "puluh.wav"
                    
                    # decode satuan
                    if(int(number)%10>0):
                        snd_str += " " + SOUND_DIRECTORY + str(int(number)%10) + ".wav"
            # decode satuan
            else:
                if(int(number)%10>0):
                    snd_str += " " + SOUND_DIRECTORY + str(int(number)%10) + ".wav"
            return snd_str            
        else:
            return None    
    except:
        pass
        logging.exception("Number to sound array error : %s", GetLastErrorMsg())
        return None

'''
combine sound using sox
'''        
def sound_combine(sound_lists=None):
    try:
        retval = os.system("/usr/bin/sox %s /dev/shm/sndx.wav" % (sound_lists,))
        if retval==0:
            return "/dev/shm/sndx.wav"
        else:
            return None
    except:
        pass
        logging.exception("Combine sound error : %s", GetLastErrorMsg())
        return None

'''
play sound file
'''       
def sound_play(sound_file=None):
    try:
        if sound_file is not None:
            retval = os.system("/usr/bin/aplay -D sysdefault:CARD=AUDIO %s -q" % (sound_file,))
            return retval
        else:
            return 255
    except:
        pass
        logging.exception("Play sound error : %s", GetLastErrorMsg())
        return 255
        
'''
make sound announcement, this is blocking
'''        
def sound_announcement(id_cso=None, number=None):
    try:
        if (int(id_cso)>0) and (int(number)>=0):
            '''
            cmdx = "/usr/bin/antrian-sounder 1 %d %d &"%(id_cso, number)
            logging.debug("Run command %s" % cmdx)
            #retval = subprocess.call(cmdx, stderr=subprocess.STDOUT)
            retval = os.system(cmdx)
            logging.debug("Sound announcement result = %d", retval)
            return retval==0
            '''
            
            '''
            # play prolog
            sound_play(SOUND_DIRECTORY + "prolog.wav")
            
            # play number line
            sound_play(number_to_sound_array(number))
            
            # play epilog
            sound_play(SOUND_DIRECTORY + "epilog.wav")
            
            # play number cso
            sound_play(number_to_sound_array(id_cso))
            
            # play ending
            sound_play(SOUND_DIRECTORY + "ending.wav")
            '''
            
            sound_play( sound_combine(SOUND_DIRECTORY + "prolog.wav " + number_to_sound_array(number) +" "+ SOUND_DIRECTORY + "epilog.wav "+number_to_sound_array(id_cso)+" "+SOUND_DIRECTORY + "ending.wav"))
            
            return True
            
        return False            
    except:
        pass
        logging.exception("Sound announcement error : %s", GetLastErrorMsg())
        return False
        
    
'''
routine data per 1 seconds
'''
def loop_main_routine():
    import logging
    logging.basicConfig(level=logging.DEBUG)
    logger_mainx = logging.getLogger("loop_main_routine")
    
    try:
        while True:
            try:
                #update display data
                osd_data = "osd/count:" + str(ANTRIAN_DB_OBJ.get_queue_list(in_csv=True))
                logger_mainx.info("Display status = %s", (osd_data))
                logger_mainx.info("Send to display = %s", (InfoBeamerUDPCommand(cmdx=osd_data)))
                
                #sleep a bit
                time.sleep(2)
            except:
                pass
                logger_mainx.exception(GetLastErrorMsg())
    except:
        pass
        logger_mainx.exception(GetLastErrorMsg())
        
    
'''
main loop here
'''
def main_loop():
    try:
        logging.debug("ANTRIAN SERVER RASPI - %s" % (APP_VERSION,))
        
        '''
        print(number_to_sound_array(1))
        print(number_to_sound_array(3))
        print(number_to_sound_array(9))
        print(number_to_sound_array(10))
        print(number_to_sound_array(11))
        print(number_to_sound_array(12))
        print(number_to_sound_array(17))
        print(number_to_sound_array(20))
        print(number_to_sound_array(23))
        print(number_to_sound_array(52))
        print(number_to_sound_array(100))
        print(number_to_sound_array(144))
        print(number_to_sound_array(543))
        print(number_to_sound_array(1000))
        print(sound_play("/data/project/info-beamer/tes/antrian-old/src/server/snd/5.wav"))        
        sound_announcement(1,23)
        sound_announcement(4,10)
        sound_announcement(2,15)
        sys.exit(0)
        '''
        
        '''
        import logging
        logging.basicConfig(level=logging.DEBUG)
        '''
        
        #print(str(datetime.datetime.strftime(datetime.datetime.now(), "%d/%m/%Y %H:%M:%S")))
        
        global PRINTER_OUTPUT
        PRINTER_OUTPUT = PRINTER_EPSON()
        
        global ANTRIAN_DB_OBJ
        ANTRIAN_DB_OBJ = DB_ANTRIAN()
        #print(ANTRIAN_DB_OBJ.get_line_not_announced_number(cso_number=1))
        
        global GPIO_OBJ
        GPIO_OBJ = GPIO_CLS(printer_output=PRINTER_OUTPUT, db_connection=ANTRIAN_DB_OBJ)    
        
        '''
        logging.debug(ANTRIAN_DB_OBJ.get_queue_list(in_csv=True))
        logging.debug(ANTRIAN_DB_OBJ.get_queue_announce_list())
        '''
        
        #spawning loop routine
        logging.debug("Started process main loop routine handler %r", spawn_new_multiprocess_action(targetx=loop_main_routine))
        
        server = Server("0.0.0.0", 50000)
        try:
            logging.info("Listening")
            server.start()
        except:
            logging.exception("Unexpected exception")
        finally:
            try:
                logging.info("Shutting down")
                for process in multiprocessing.active_children():
                    logging.info("Shutting down process %r", process)
                    process.terminate()
                    process.join()
            except:
                pass
                logging.exception(GetLastErrorMsg())
    except:
        pass
        logging.exception(GetLastErrorMsg())
    
    # cleanup
    ANTRIAN_DB_OBJ.close()
    GPIO_OBJ.cleaning()
    logging.info("All done")
    
'''
main program start here
'''
if __name__ == "__main__":
    main_loop()
