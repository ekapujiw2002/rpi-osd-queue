#!/bin/bash
python2 -m PyInstaller --strip --onefile --clean --hidden-import=six --hidden-import=appdirs --hidden-import=packaging --hidden-import=packaging --hidden-import=packaging.version --hidden-import=packaging.specifiers --hidden-import=packaging.requirements $@
