'''
https://gist.github.com/micktwomey/606178
compile to one executable using :
python2 -m PyInstaller --strip --onefile --clean --hidden-import=six --hidden-import=appdirs --hidden-import=packaging --hidden-import=packaging --hidden-import=packaging.version --hidden-import=packaging.specifiers --hidden-import=packaging.requirements
'''

import sys
import multiprocessing
import socket
import time, datetime
from string import Formatter

#db
import sqlite3, json

#gpio
import RPi.GPIO as GPIO

#printer
from escpos import constants, escpos, exceptions, printer
from escpos.printer import File
#import escpos

'''
info beamer port udp
'''
INFO_BEAMER_UDP = {
	'HOST': '127.0.0.1',
	'PORT': 4444
}
INFO_BEAMER_SOCKET_ADDR = (INFO_BEAMER_UDP['HOST'],INFO_BEAMER_UDP['PORT'])
# info beamer udp socket
INFO_BEAMER_SOCKET = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

# global vars
PRINTER_OUTPUT = None
ANTRIAN_DB_OBJ = None
GPIO_OBJ = None

'''
	get last error msg
'''
def GetLastErrorMsg():
	return sys.exc_info()[1]
	
'''
Info beamer udp command
'''
def InfoBeamerUDPCommand(cmdx=None):
	err_val=0
	err_msg=''
	try:
		if cmdx is not None:
			sent = INFO_BEAMER_SOCKET.sendto(cmdx,INFO_BEAMER_SOCKET_ADDR)
			if sent == len(cmdx):
				err_msg = 'OK'
				err_val = 0
			else:
				err_msg = 'Not all message sent'
				err_val = 1
		else:
			err_msg = 'No command'
			err_val = 1
	except:
		pass
		err_msg = GetLastErrorMsg()
		err_val = 1
		
	return (err_val,err_msg)

'''
https://stackoverflow.com/a/42320260
https://stackoverflow.com/questions/538666/python-format-timedelta-to-string
'''
def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02}s', inputtype='timedelta'):
	"""Convert a datetime.timedelta object or a regular number to a custom-
	formatted string, just like the stftime() method does for datetime.datetime
	objects.

	The fmt argument allows custom formatting to be specified.  Fields can 
	include seconds, minutes, hours, days, and weeks.  Each field is optional.

	Some examples:
		'{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
		'{W}w {D}d {H}:{M:02}:{S:02}'	 --> '4w 5d 8:04:02'
		'{D:2}d {H:2}:{M:02}:{S:02}'	  --> ' 5d  8:04:02'
		'{H}h {S}s'					   --> '72h 800s'

	The inputtype argument allows tdelta to be a regular number instead of the  
	default, which is a datetime.timedelta object.  Valid inputtype strings: 
		's', 'seconds', 
		'm', 'minutes', 
		'h', 'hours', 
		'd', 'days', 
		'w', 'weeks'
	"""

	# Convert tdelta to integer seconds.
	if inputtype == 'timedelta':
		remainder = int(tdelta.total_seconds())
	elif inputtype in ['s', 'seconds']:
		remainder = int(tdelta)
	elif inputtype in ['m', 'minutes']:
		remainder = int(tdelta)*60
	elif inputtype in ['h', 'hours']:
		remainder = int(tdelta)*3600
	elif inputtype in ['d', 'days']:
		remainder = int(tdelta)*86400
	elif inputtype in ['w', 'weeks']:
		remainder = int(tdelta)*604800

	f = Formatter()
	desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
	possible_fields = ('W', 'D', 'H', 'M', 'S')
	constants = {'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
	values = {}
	for field in possible_fields:
		if field in desired_fields and field in constants:
			values[field], remainder = divmod(remainder, constants[field])
	return f.format(fmt, **values)
	
'''
read end file
'''
def read_end_datetime_file():
	try:
		try:
			file = open("/mnt/data/endtime.txt","r")
		except:
			pass
			
		try:
			file = open("/etc/countdown/endtime.txt","r")
		except:
			pass
			
		try:
			file = open("endtime.txt","r")
		except:
			pass
			
		strx = file.read()
		file.close()
		strx = strx.rstrip("\r\n")
		return strx
	except:
		pass
		return "2018-8-18 0:0:0"

'''
get day diff time
'''
def get_delta_days_time():
	try:
		d0 = datetime.datetime.strptime(read_end_datetime_file(),"%Y-%m-%d %H:%M:%S")
		d1 = datetime.datetime.now()
		return strfdelta(d0-d1,"{D},{H},{M},{S}")
	except:
		pass
		return "0,0,0,0"

'''
main connection handler
'''
def server_connection_handler(connection, address):
	import logging
	logging.basicConfig(level=logging.DEBUG)
	logger = logging.getLogger("process-%r" % (address,))
	try:
		logger.debug("Connected %r at %r", connection, address)
		while True:
			'''
			data = connection.recv(1024)
			if data == "":
				logger.debug("Socket closed remotely")
				break
			logger.debug("Received data %r", data)
			'''
			'''
			connection.sendall(data)
			connection.sendall(get_delta_days_time())
			logger.debug("Sent data")
			'''
			
			data = connection.recv(1024)
			if data == "":
				logger.debug("Socket closed remotely")
				break
			logger.debug("Received data %r", data)
			
			try:
				#split the command
				cmd_detil = data.split(',')
				logger.debug("Parsed command = %r", cmd_detil)
				
				cmd_head = cmd_detil[0]
								
				#command request nomor 
				if cmd_head == 'c':
					logger.debug("Request new number for LINE %s", cmd_detil[1])
					
				#command recall
				if cmd_head == 'r':
					logger.debug("Recall for LINE %s", cmd_detil[1])
					
			except:
				pass
				logger.exception("Got error handling the command")
			#time.sleep(0.05)
	except:
		logger.exception("Problem handling request")
	finally:
		logger.debug("Closing socket")
		connection.close()

'''
main server class for clients
'''
class Server(object):
	'''
	init the server
	'''
	def __init__(self, hostname, port):
		import logging
		self.logger = logging.getLogger("server")
		self.hostname = hostname
		self.port = port

	'''
	start the server as daemon
	'''
	def start(self):
		self.logger.debug("listening")
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.bind((self.hostname, self.port))
		self.socket.listen(1)

		while True:
			conn, address = self.socket.accept()
			self.logger.debug("Got connection")
			process = multiprocessing.Process(target=server_connection_handler, args=(conn, address))
			process.daemon = True
			process.start()
			self.logger.debug("Started process %r", process)
		
'''
gpio handler class
'''		
class GPIO_CLS(object):
	def __init__(self, printer_output=None, db_connection=None):
		import logging
		self.logger = logging.getLogger("gpio")
		self.is_printing = False
			
		try:
			GPIO.setwarnings(False)
			GPIO.setmode(GPIO.BCM)
			GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP)
			GPIO.add_event_detect(26, GPIO.FALLING, callback=self.gpio_switch_callback, bouncetime=200)
			self.printer_output = printer_output
			self.db_connection = db_connection
			
			self.logger.debug("GPIO init OK")
		except:
			pass
			self.logger.exception("GPIO init fail at %s" % (GetLastErrorMsg()))
		
	'''
	cleanup
	'''
	def cleaning(self):
		try:
			GPIO.cleanup()
			self.logger.debug("GPIO cleanup OK")
		except:
			pass
			self.logger.exception("GPIO cleanup fail at %s" % (GetLastErrorMsg()))
	
	'''
	when user press button
	'''
	def gpio_switch_callback(self, channel):
		try:
			if not self.is_printing:
				self.is_printing = True
				self.logger.debug("GPIO start printing...")
				
				print("%f : GPIO %s pressed" % (time.time(), channel))
				new_queue_number = self.db_connection.get_queue_number(pool_num=1)
				if new_queue_number != -1:
					self.printer_output.print_note(numberx=channel)
					self.logger.debug("GPIO printing OK")
				else:
					self.logger.exception("GPIO printing fail at requesting new queue number")				
				
				self.is_printing = False
		except:
			pass
			self.is_printing = False
			self.logger.exception("GPIO printing fail at %s" % (GetLastErrorMsg()))
		
'''
printing struk class
'''
class PRINTER_EPSON(object):
	def __init__(self,printer_dev_name="/dev/usb/lp0"):
		import logging
		self.logger = logging.getLogger("printer")
		
		try:
			self.printer_obj = printer.File(printer_dev_name)
			self.logger.debug("Printer init OK")
		except:
			pass
			self.printer_obj = None
			self.logger.exception("Printer init fail at %s" % (GetLastErrorMsg()))
					
	def print_note(self,numberx=0):
		try:
			self.logger.debug("Printer start printing")
			
			if self.printer_obj is not None:
				self.printer_obj.text("HELLO WORLD\n")
				self.printer_obj.cut()
			else:
				self.logger.warning("Printer not opened!!!")
			
			self.logger.debug("Printer end printing")
			
			return True
		except:
			pass
			self.logger.exception("Printer note fail at %s" % (GetLastErrorMsg()))
			return False
			
# db class antrian
# https://stackoverflow.com/questions/393554/python-sqlite3-and-concurrency
class DBAntrian(object):
	#def __init__(self, driver='mysql', hostname='localhost', username='admin_db', password='1234', dbname='raspi-fish-feeder'):
	def __init__(self, driver='sqlite', hostname=None, username=None, password=None, dbname='antrian.db'):
		import logging
		logging.basicConfig()
		
		self.logger = logging.getLogger("db")
		self.logger.debug("Initializing...")
		
		try:
			self.type = driver
			self.dbname = dbname
			
			if self.type == 'sqlite':
				self.con = sqlite3.connect(self.dbname, check_same_thread = False)
				
			if self.type == 'mysql':
				self.con = pymysql.connect(hostname,username,password,dbname)
			
			self.init_success = True
		except:
			pass
			self.init_success = False
			
		self.logger.debug("Init result %s" % (self.init_success))
		   
	def close(self):
		try:
			self.logger.debug("Close database OK")
			
			#if self.type == 'sqlite':
			self.con.close()
				
			#if self.type == 'mysql':
			#	self.con.close()
		except:
			pass
			self.logger.exception("Close database fail at %s" % (GetLastErrorMsg()))
		   
	'''
	def get_schedule_list(self, in_json = False):
		try:			
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute('SELECT * FROM feed_schedule ORDER BY id')
				rows = cur.fetchall()
					
				if in_json:
					return json.dumps([dict(ix) for ix in rows])
				return [dict(ix) for ix in rows]
			return None
		except:
			pass
			return None		
	#https://stackoverflow.com/questions/21986194/how-to-pass-dictionary-items-as-function-arguments-in-python
	def update_schedule(self, id=None, aktif=0, senin=0, selasa=0, rabu=0, kamis=0, jumat=0, sabtu=0, minggu=0, jam='00:00:00', berat=0.0):
		try:
			if self.init_success and (id is not None):
				cur = self.con.cursor()
				cur.execute('update feed_schedule set aktif=?, senin=?, selasa=?, rabu=?, kamis=?, jumat=?, sabtu=?, minggu=?, jam=?, berat=? where id=?',(aktif, senin, selasa, rabu, kamis, jumat, sabtu, minggu, jam, berat, id))
				self.con.commit()
				return cur.rowcount
			return 0
		except:
			pass
			return 0
	'''
			
	# get new line up number
	def get_queue_number(self, pool_num=None):
		try:
			new_number = -1
			if self.init_success and (pool_num is not None):
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				# check for pool data
				cur.execute("select * from master_nomor where pool=?", (str(pool_num)))
				rows = cur.fetchall()
				#print(rows)
				
				# got data
				if len(rows) != 0:
					# got pool data, check the date
					cur.execute("select * from master_nomor where pool=? and date(waktu,'localtime')=date('now','localtime') order by waktu desc limit 1", (str(pool_num)))
					rows = cur.fetchall()
					#print(rows)
					#print(json.dumps([dict(ix) for ix in rows]))
					
					#already have the data, increase the number
					if len(rows) !=0:
						#old_number = int(rows[0]['nomor'])
						new_number = int(rows[0]['nomor']) + 1
						
						#update items
						cur.execute("update master_nomor set nomor=?, waktu=datetime('now','localtime') where pool=?", (new_number,pool_num))
						self.con.commit()
						
						#print(new_number)
					
					#init the data for today
					else:
						cur.execute("insert or replace into master_nomor(pool,nomor) values(?,1)", str(pool_num))
						self.con.commit()
						new_number = 1
					
					
				# no data, insert new one
				else:
					cur.execute("insert into master_nomor(pool,nomor) values(?,1)", (str(pool_num)))
					self.con.commit()
					new_number = 1
					
			#return new_number
			self.logger.debug("Request new number OK with result %d" % (new_number))
				
		except:
			pass
			#print(GetLastErrorMsg())
			self.logger.exception("Request new number fail at %s" % (GetLastErrorMsg()))
			new_number = -1
			
		return new_number
		
	'''
	get all queue list
	'''
	def get_queue_list(self, in_json = False, in_csv = False):
		try:			
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute("""
					SELECT a.*, ifnull(b.nomor,0) nomor, ifnull(b.waktu,datetime('now','localtime')) waktu, ifnull(b.status,0) status
					FROM master_cso a
					LEFT JOIN log_cso b
					ON a.id_cso = b.id_cso AND date(b.waktu,'localtime')=date('now','localtime')
					ORDER BY a.id_cso ASC
				""")
				rows = cur.fetchall()
				
				# as csv
				if in_csv:
					resx = ""
					for ix in rows:
						resx += str(ix['nomor'])+","
					return resx[:-1]
					
				# as json
				if in_json:
					return json.dumps([dict(ix) for ix in rows])
					
				# as dictionary
				return [dict(ix) for ix in rows]
			return None
		except:
			pass
			return None

	'''
	get all queue need announcement
	'''
	def get_queue_announce_list(self):
		try:
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute("""
					select no_urut,id_cso,nomor from log_cso where date(waktu,'localtime')=date('now','localtime') and status=0 order by id_cso
				""")
				rows = cur.fetchall()
				# as dictionary
				return [dict(ix) for ix in rows]
			
			return None
		except:
			pass
			return None
			
	'''
	update status nomor cso
	'''
	def update_cso_queue_status(self,new_status=None,data_key=None):
		try:
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute("""
					update log_cso
					set status=?
					where no_urut=?
				""", (new_status, data_key))
				self.con.commit()
				
				#print(cur.rowcount)
				
				# as dictionary
				return cur.rowcount==1
			
			return False
		except:
			pass
			return False
			
	'''
	allocate new number from master_cso to log_cso for a line
	'''
	def get_line_number_allocation(self, lane_number=None):
		try:
			new_number = -1
			if self.init_success:
				if self.type == 'sqlite':
					self.con.row_factory = sqlite3.Row
					cur = self.con.cursor()					
					
				if self.type == 'mysql':
					#https://stackoverflow.com/questions/4940670/pymysql-fetchall-results-as-dictionary
					cur = self.con.cursor(pymysql.cursors.DictCursor)
				
				cur.execute("""
					update log_cso
					set status=?
					where no_urut=?
				""", (new_status, data_key))
				self.con.commit()
				
				#print(cur.rowcount)
				
				# as dictionary
				return cur.rowcount==1
			
		except:
			pass
			new_number = -1
		
		return new_number
	
'''
main loop here
'''
def main_loop():
	import logging
	logging.basicConfig(level=logging.DEBUG)
	
	global PRINTER_OUTPUT
	PRINTER_OUTPUT = PRINTER_EPSON()
	
	global ANTRIAN_DB_OBJ
	ANTRIAN_DB_OBJ = DBAntrian()
	
	global GPIO_OBJ
	GPIO_OBJ = GPIO_CLS(printer_output=PRINTER_OUTPUT, db_connection=ANTRIAN_DB_OBJ)	
	
	'''
	logging.debug(ANTRIAN_DB_OBJ.get_queue_list(in_csv=True))
	logging.debug(ANTRIAN_DB_OBJ.get_queue_announce_list())
	'''
	
	server = Server("0.0.0.0", 50000)
	try:
		logging.info("Listening")
		server.start()
	except:
		logging.exception("Unexpected exception")
	finally:
		logging.info("Shutting down")
		for process in multiprocessing.active_children():
			logging.info("Shutting down process %r", process)
			process.terminate()
			process.join()
	
	# cleanup
	ANTRIAN_DB_OBJ.close()
	GPIO_OBJ.cleaning()
	logging.info("All done")
	
'''
main program start here
'''
if __name__ == "__main__":
	main_loop()
