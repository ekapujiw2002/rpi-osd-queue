#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#include <RTClib.h>
#include <Wire.h>

// fitur device
// webserver debugger
// #define WEBSERVER_DEBUG_ON
// #define WEBSERVER_DEBUG_PORT 8080
#ifdef WEBSERVER_DEBUG_ON
#include <ESP8266WebServer.h>
#endif

// telnet debugger
// #define TELNET_DEBUG_ON
#ifdef TELNET_DEBUG_ON
#include <RemoteDebug.h>
#endif

// debug port
#define APP_DEBUG_PORT Serial
#define APP_DEBUG_SPEED 115200

// ssid dan pass ap wifi
const char *ssid = "ROBOTIC_ROOM";
const char *password = "Robot09876543212017";

// soft ap password
const char *ap_password = "12345678902017";

// static ip assigned
// https://community.blynk.cc/t/setting-staticip-address-on-esp8266/7866/11
IPAddress DEV_STA_IP(192, 168, 1, 200), DEV_STA_GATEWAY(192, 168, 1, 1),
    DEV_STA_SUBNET(255, 255, 255, 0), DEV_STA_DNS(192, 168, 1, 1);

IPAddress DEV_SOFTAP_IP(10, 0, 0, 1), DEV_SOFTAP_GATEWAY(10, 0, 0, 1),
    DEV_SOFTAP_SUBNET(255, 255, 255, 0);

// simple http server to set or reset the device
#ifdef WEBSERVER_DEBUG_ON
ESP8266WebServer HTTP_SERVER(WEBSERVER_DEBUG_PORT);
#endif

// telnet debugger
#ifdef TELNET_DEBUG_ON
#define TELNET_DEBUGGER_HOSTNAME "ex4-telnet-dev"
RemoteDebug TELNET_DEBUGGER;
#endif

// udp socket ntp server
WiFiUDP NTP_UDP_SERVER;
uint32_t RTCcounts;
unsigned int NTP_UDP_LOCAL_PORT = 123; // local port to listen on
// char NTP_UDP_PACKET_BUFFER[64];        // buffer for incoming packets
// char replyPacekt[] =
//     "Hi there! Got the message :-)"; // a reply string to send back

// local rtc device
RTC_DS3231 DEV_RTC_LOCAL;
DateTime RTC_CURRENT_DATETIME = 0;
String RTC_CURRENT_DATETIME_STR = "";
const char daysOfTheWeek[7][12] = {"Sunday",   "Monday", "Tuesday", "Wednesday",
                                   "Thursday", "Friday", "Saturday"};

uint32_t t_now;

// proto void
void APP_DEBUG_PRINT(String alog);
void APP_DEBUG_PRINT(String alog);

#ifdef TELNET_DEBUG_ON
void telnet_debugger_initialize();
void telnet_process_remote_command();
#endif

/**
 * Calculate seconds since 1900
 * @method SecondsSince1900
 * @param  year             year > 2015
 * @param  month            month = 1 to 12
 * @param  day              day = 1 to 31
 * @param  hour             hour = 0 to 23
 * @param  minute           minute = 0 to 59
 * @param  second           second = 0 to 59
 * @return                  seconds from 1900
 */
uint32 ICACHE_FLASH_ATTR SecondsSince1900(uint16 year, uint8 month, uint8 day,
                                          uint8 hour, uint8 minute,
                                          uint8 second);
void dev_rtc_get_current_datetime();
void ntp_request_processed();
void wifi_ap_sta_initialize();
String getValue(String data, const int index, const char separator);

/**
 * init debug port serial if any
 * @method APP_DEBUG_INIT
 */
void APP_DEBUG_INIT() {
#ifdef APP_DEBUG_PORT
  APP_DEBUG_PORT.begin(APP_DEBUG_SPEED);
  APP_DEBUG_PORT.setDebugOutput(true);
#endif
}

/**
 * debug printing util
 * @method APP_DEBUG_PRINT
 * @param  alog            [description]
 */

void APP_DEBUG_PRINT(String alog) {
#ifdef APP_DEBUG_PORT
  char dtx[64] = {0};
  snprintf_P(dtx, sizeof(dtx), (const char *)F("%-10u : "), millis());
  APP_DEBUG_PORT.println(String(dtx) + alog);
#endif
}

/**
 * get value from string delimited by char
 * @method getValue
 * @param  data      the string input_string
 * @param  separator char separator data
 * @param  index     index data from 0
 * @return           string
 */
String getValue(String data, const int index, const char separator) {
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;
  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

/******************************************************************************
 * FunctionName : SecondsSince1900
 * Description  : Returns input date in seconds since 00:00 GMT JAN 01 1900
 *                			No range verification! use with care!
 * Parameters   : year > 2015, month (1-12), day (1-31), hour (0-23), minute
 *(0-59), second (0-59)
 * Returns      : Integer 32 bits
 *******************************************************************************/
uint32 ICACHE_FLASH_ATTR SecondsSince1900(uint16 year, uint8 month, uint8 day,
                                          uint8 hour, uint8 minute,
                                          uint8 second) {
  uint16 counter;
  uint16 yearcount, leapdays;
  uint32 daysfromepoch;
  uint16 daysUpToMonth[12] = {0,   31,  59,  90,  120, 151,
                              181, 212, 243, 273, 304, 334};
  uint16 daysUpToMonthLeapYear[12] = {0,   31,  60,  91,  121, 152,
                                      182, 213, 244, 274, 305, 335};

  // integer years from epoch
  yearcount = year - 1900;

  // get leapdays from epoch
  leapdays = 0;
  for (counter = 1900; counter < year; counter++) {
    if (((counter % 4 == 0) && (counter % 100 != 0)) || (counter % 400 == 0)) {
      leapdays++;
    }
  }

  if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
    daysfromepoch = yearcount * 365 + leapdays +
                    daysUpToMonthLeapYear[month - 1] + (day - 1);
  } else {
    daysfromepoch =
        yearcount * 365 + leapdays + daysUpToMonth[month - 1] + (day - 1);
  }

  return ((daysfromepoch * 86400) + (hour * 3600) + (minute * 60) + second);
}

/**
 * read current datetime from rtc
 * @method dev_rtc_get_current_datetime
 */
void dev_rtc_get_current_datetime() {
  static uint32_t t_last_rtc_read = 0;
  uint32_t tnx = millis();

  if (tnx - t_last_rtc_read >= 1000) {
    t_last_rtc_read = tnx;
    RTCcounts = micros();
    RTC_CURRENT_DATETIME = DEV_RTC_LOCAL.now();

    RTC_CURRENT_DATETIME_STR =
        String(daysOfTheWeek[RTC_CURRENT_DATETIME.dayOfTheWeek()]) +
        String(", ") + String(RTC_CURRENT_DATETIME.day()) + String("-") +
        String(RTC_CURRENT_DATETIME.month()) + String("-") +
        String(RTC_CURRENT_DATETIME.year()) + String(" ") +
        String(RTC_CURRENT_DATETIME.hour()) + String(":") +
        String(RTC_CURRENT_DATETIME.minute()) + String(":") +
        String(RTC_CURRENT_DATETIME.second());
    APP_DEBUG_PRINT(RTC_CURRENT_DATETIME_STR);
    /*
        Serial.print(RTC_CURRENT_DATETIME.year(), DEC);
        Serial.print('/');
        Serial.print(RTC_CURRENT_DATETIME.month(), DEC);
        Serial.print('/');
        Serial.print(RTC_CURRENT_DATETIME.day(), DEC);
        Serial.print(" (");
        Serial.print(daysOfTheWeek[RTC_CURRENT_DATETIME.dayOfTheWeek()]);
        Serial.print(") ");
        Serial.print(RTC_CURRENT_DATETIME.hour(), DEC);
        Serial.print(':');
        Serial.print(RTC_CURRENT_DATETIME.minute(), DEC);
        Serial.print(':');
        Serial.print(RTC_CURRENT_DATETIME.second(), DEC);
        Serial.println();
        */
  }
}

/**
 * process ntp request
 * @method ntp_request_processed
 */
void ntp_request_processed() {
  int packetSize = NTP_UDP_SERVER.parsePacket();
  // ntp udp packet size is 48byte
  if (packetSize > 47) {
    char ntp_packet[48];
    uint32 timetmp;
    uint32 fractmp;
    uint32 useconds;

    // copy to local
    int len = NTP_UDP_SERVER.read(ntp_packet, packetSize);
    // Serial.printf("NTP request from %s:%d\n",
    //               NTP_UDP_SERVER.remoteIP().toString().c_str(),
    //               NTP_UDP_SERVER.remotePort());

    APP_DEBUG_PRINT(String("NTP request from ") +
                    NTP_UDP_SERVER.remoteIP().toString() + String(":") +
                    String(NTP_UDP_SERVER.remotePort()));

    // update the data
    // NTP values!!!

    // if(GPSfix==1)
    // {
    // 	ntp_packet[0]=0x1C;	// Leap 0x0, Version 0x3, Mode 0x4
    // }
    // else
    // {
    // 	ntp_packet[0]=0xDC;	// Leap 0x3, Version 0x3, Mode 0x4
    // }
    ntp_packet[0] = 0x1C; // Leap 0x0, Version 0x3, Mode 0x4

    ntp_packet[1] = 0x01; // Stratum 0x1
    ntp_packet[2] = 0x03; // Poll  0x3 (invalid)
    ntp_packet[3] = 0xE3; // Precision  0,00000 sec?

    ntp_packet[4] = 0; // Root Delay?
    ntp_packet[5] = 0; // Root Delay?
    ntp_packet[6] = 0; // Root Delay?
    ntp_packet[7] = 0; // Root Delay?

    ntp_packet[8] = 0;  // Root Dispersion?
    ntp_packet[9] = 0;  // Root Dispersion?
    ntp_packet[10] = 0; // Root Dispersion?
    ntp_packet[11] = 0; // Root Dispersion?

    ntp_packet[12] = 'E';  // Reference ID, "E"
    ntp_packet[13] = 'X';  // Reference ID, "X"
    ntp_packet[14] = '4';  // Reference ID, "4"
    ntp_packet[15] = 0x00; // Reference ID, 0x00

    timetmp = SecondsSince1900(
        RTC_CURRENT_DATETIME.year(), RTC_CURRENT_DATETIME.month(),
        RTC_CURRENT_DATETIME.day(), RTC_CURRENT_DATETIME.hour(),
        RTC_CURRENT_DATETIME.minute(), RTC_CURRENT_DATETIME.second());

    ntp_packet[16] =
        (char)(0x000000FF & timetmp >> 24); // Reference timestamp seconds
    ntp_packet[17] =
        (char)(0x000000FF & timetmp >> 16); // Reference timestamp seconds
    ntp_packet[18] =
        (char)(0x000000FF & timetmp >> 8); // Reference timestamp seconds
    ntp_packet[19] =
        (char)(0x000000FF & timetmp); // Reference timestamp seconds

    ntp_packet[20] = 0x0; // Reference timestamp fraction
    ntp_packet[21] = 0x0; // Reference timestamp fraction
    ntp_packet[22] = 0x0; // Reference timestamp fraction
    ntp_packet[23] = 0x0; // Reference timestamp fraction

    // Origin timestamp [24]..[27] seconds , [28]..[31] fraction

    ntp_packet[24] = ntp_packet[40];
    ntp_packet[25] = ntp_packet[41];
    ntp_packet[26] = ntp_packet[42];
    ntp_packet[27] = ntp_packet[43];

    ntp_packet[28] = ntp_packet[44];
    ntp_packet[29] = ntp_packet[45];
    ntp_packet[30] = ntp_packet[46];
    ntp_packet[31] = ntp_packet[47];

    timetmp = SecondsSince1900(
        RTC_CURRENT_DATETIME.year(), RTC_CURRENT_DATETIME.month(),
        RTC_CURRENT_DATETIME.day(), RTC_CURRENT_DATETIME.hour(),
        RTC_CURRENT_DATETIME.minute(), RTC_CURRENT_DATETIME.second());

    fractmp = micros();
    if (fractmp >= RTCcounts) {
      useconds = fractmp - RTCcounts;
    } else {
      useconds = (0xFFFFFFFF - RTCcounts) + fractmp;
    }

    // useconds = useconds + GPRMC_US_OFFSET_BYTETIME;
    // useconds = 0;

    ntp_packet[32] =
        (char)(0x000000FF & timetmp >> 24); // Receive timestamp seconds
    ntp_packet[33] =
        (char)(0x000000FF & timetmp >> 16); // Receive timestamp seconds
    ntp_packet[34] =
        (char)(0x000000FF & timetmp >> 8);         // Receive timestamp seconds
    ntp_packet[35] = (char)(0x000000FF & timetmp); // Receive timestamp seconds

    ntp_packet[36] =
        (char)(0x000000FF & useconds >> 24); // Receive timestamp fraction
    ntp_packet[37] =
        (char)(0x000000FF & useconds >> 16); // Receive timestamp fraction
    ntp_packet[38] =
        (char)(0x000000FF & useconds >> 8); // Receive timestamp fraction
    ntp_packet[39] =
        (char)(0x000000FF & useconds); // Receive timestamp fraction

    timetmp = SecondsSince1900(
        RTC_CURRENT_DATETIME.year(), RTC_CURRENT_DATETIME.month(),
        RTC_CURRENT_DATETIME.day(), RTC_CURRENT_DATETIME.hour(),
        RTC_CURRENT_DATETIME.minute(), RTC_CURRENT_DATETIME.second());

    fractmp = micros();
    if (fractmp >= RTCcounts) {
      useconds = fractmp - RTCcounts;
    } else {
      useconds = (0xFFFFFFFF - RTCcounts) + fractmp;
    }

    ntp_packet[40] =
        (char)(0x000000FF & timetmp >> 24); // Transmit timestamp seconds
    ntp_packet[41] =
        (char)(0x000000FF & timetmp >> 16); // Transmit timestamp seconds
    ntp_packet[42] =
        (char)(0x000000FF & timetmp >> 8);         // Transmit timestamp seconds
    ntp_packet[43] = (char)(0x000000FF & timetmp); // Transmit timestamp seconds

    ntp_packet[44] =
        (char)(0x000000FF & useconds >> 24); // Transmit timestamp fraction
    ntp_packet[45] =
        (char)(0x000000FF & useconds >> 16); // Transmit timestamp fraction
    ntp_packet[46] =
        (char)(0x000000FF & useconds >> 8); // Transmit timestamp fraction
    ntp_packet[47] =
        (char)(0x000000FF & useconds); // Transmit timestamp fraction

    // sent it back
    NTP_UDP_SERVER.beginPacket(NTP_UDP_SERVER.remoteIP(),
                               NTP_UDP_SERVER.remotePort());
    NTP_UDP_SERVER.write(ntp_packet, 48);
    NTP_UDP_SERVER.endPacket();
  }
}

/**
 * init wifi as ap and client
 * @method wifi_ap_sta_initialize
 */
void wifi_ap_sta_initialize() {
  // setup wifi as station and soft ap
  APP_DEBUG_PRINT(String("Connecting to ") + String(ssid));

  // ap name
  String ap_name = String("AP-") + String(ESP.getChipId(), HEX) +
                   String(ESP.getFlashChipId(), HEX);

  // no wifi setting are saved to flash
  WiFi.persistent(false);

  // set hostname
  WiFi.hostname(ap_name);

  // set to ap and station wifi
  WiFi.mode(WIFI_AP_STA);

  // assign static ip to ap
  if (WiFi.softAPConfig(DEV_SOFTAP_IP, DEV_SOFTAP_GATEWAY, DEV_SOFTAP_SUBNET)) {
    APP_DEBUG_PRINT("Soft AP IP set OK");
    if (WiFi.softAP(ap_name.c_str(), ap_password)) {
      APP_DEBUG_PRINT(String("Init Soft AP Wifi OK. IP = ") +
                      WiFi.softAPIP().toString());
    } else {
      APP_DEBUG_PRINT("Init Soft AP Wifi ERROR");
    }
  } else {
    APP_DEBUG_PRINT("Soft AP IP set ERROR");
  }

  // connect to designated wifi ap
  if (WiFi.config(DEV_STA_IP, DEV_STA_GATEWAY, DEV_STA_SUBNET, DEV_STA_DNS)) {
    WiFi.begin(ssid, password);
    int cntmax = 0;
    while ((WiFi.status() != WL_CONNECTED) && (cntmax < 20)) {
      delay(500);
      cntmax++;
      APP_DEBUG_PRINT(".");
    }
    if (cntmax < 20) {
      APP_DEBUG_PRINT("Connected");
    } else {
      APP_DEBUG_PRINT("Connection fail");
    }
  }

  // start ntp server
  NTP_UDP_SERVER.begin(NTP_UDP_LOCAL_PORT);
  APP_DEBUG_PRINT(String("Now listening at IP (") + WiFi.localIP().toString() +
                  String(" , ") + WiFi.softAPIP().toString() + String("):") +
                  NTP_UDP_LOCAL_PORT);
}

/**
 * WEB SERVER IMPLEMENTATION
 */
#ifdef WEBSERVER_DEBUG_ON
/**
 * handler root http
 * @method http_root_handler
 */
void http_root_handler() {
  HTTP_SERVER.send(200, "text/plain", "EX4 NTP SERVER");
}

/**
 * handler api
 * @method http_api_rtc_handler
 */
void http_api_rtc_handler() {
  String message = "You send : \n";
  if (HTTP_SERVER.args() > 0) {
    for (uint8_t i = 0; i < HTTP_SERVER.args(); i++) {
      message +=
          " " + HTTP_SERVER.argName(i) + ": " + HTTP_SERVER.arg(i) + "\n";
    }
  }
  HTTP_SERVER.send(200, "text/plain", message);
  // delete &message;
  message = "";
}

/**
 * http on not found
 * @method http_not_found_handler
 */
void http_not_found_handler() {
  HTTP_SERVER.send(404, "text/plain", "INVALID REQUEST");
}

/**
 * init http server
 * @method http_server_initialize
 */
void http_server_initialize() {
  HTTP_SERVER.on("/", http_root_handler);
  HTTP_SERVER.on("/rtc", http_api_rtc_handler);
  HTTP_SERVER.onNotFound(http_not_found_handler);
  HTTP_SERVER.begin();
}
#endif

#ifdef TELNET_DEBUG_ON
void telnet_debugger_initialize() {
  TELNET_DEBUGGER.begin(TELNET_DEBUGGER_HOSTNAME);
  TELNET_DEBUGGER.setResetCmdEnabled(true);
  TELNET_DEBUGGER.showDebugLevel(true); // To not show debug levels
  TELNET_DEBUGGER.showTime(true);       // To show time
  // TELNET_DEBUGGER.showProfiler(true); // To show profiler - time between
  // messages of
  // Debug
  // Good to "begin ...." and "end ...." messages

  TELNET_DEBUGGER.showProfiler(true); // Profiler
  TELNET_DEBUGGER.showColors(true);   // Colors

  // TELNET_DEBUGGER.setSerialEnabled(true); // if you wants serial echo - only
  // recommended if ESP8266 is plugged in USB

  String helpCmd = "SET RTC yyyy-mm-dd-hh-nn-ss\t: Set RTC date time. Date "
                   "time must be in UTC!!!\r\n";
  helpCmd.concat("GET RTC\t: Get RTC current date time");

  TELNET_DEBUGGER.setHelpProjectsCmds(helpCmd);
  TELNET_DEBUGGER.setCallBackProjectCmds(&telnet_process_remote_command);
}

/**
 * handle command from telnet
 * @method telnet_process_remote_command
 */
void telnet_process_remote_command() {
  String lastCmd = TELNET_DEBUGGER.getLastCommand(), cmd_param;
  int cmd_index = -1;

  APP_DEBUG_PRINT(lastCmd);
  if (TELNET_DEBUGGER.isActive(TELNET_DEBUGGER.ANY)) {
    TELNET_DEBUGGER.println(lastCmd);

    if ((cmd_index = lastCmd.indexOf("SET RTC")) > -1) {
      cmd_param = lastCmd.substring(cmd_index + String("SET RTC").length());
      cmd_index = 0;
    } else if ((cmd_index = lastCmd.indexOf("GET RTC")) > -1) {
      cmd_param = lastCmd.substring(cmd_index + String("GET RTC").length());
      cmd_index = 1;
    } else {
      cmd_index = -1;
      cmd_param = "";
    }

    cmd_param.trim();
    APP_DEBUG_PRINT(String(cmd_index) + String(" -- ") + cmd_param);
    switch (cmd_index) {
    case 0:
      // yyyy-mm-dd-hh-nn-ss
      uint16_t yr, mnth, dy, hr, mn, sc;
      yr = getValue(cmd_param, 0, '-').toInt();
      mnth = getValue(cmd_param, 1, '-').toInt();
      dy = getValue(cmd_param, 2, '-').toInt();
      hr = getValue(cmd_param, 3, '-').toInt();
      mn = getValue(cmd_param, 4, '-').toInt();
      sc = getValue(cmd_param, 5, '-').toInt();

      DEV_RTC_LOCAL.adjust(DateTime(yr, mnth, dy, hr, mn, sc));
      TELNET_DEBUGGER.printf("Set RTC to : %u-%u-%u %u:%u:%u\r\n", yr, mnth, dy,
                             hr, mn, sc);
      TELNET_DEBUGGER.println("Set RTC OK. Please read again");
      break;

    case 1:
      TELNET_DEBUGGER.println(RTC_CURRENT_DATETIME_STR);
      break;

    default:
      break;
    }
  }
}
#endif

/**
 * init led debug
 * @method led_sign_initialize
 */
void led_sign_initialize() {
  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, LOW);
}

/**
 * led debug toggle
 * @method led_sign_toggle
 * @param  toggle_rate     toggle rate at ms
 */
void led_sign_toggle(uint16_t toggle_rate = 1000) {
  static uint32_t t_last_led_buildin_on = 0;
  uint32_t tnx = millis();
  if (tnx - t_last_led_buildin_on >= toggle_rate) {
    t_last_led_buildin_on = tnx;
    digitalWrite(BUILTIN_LED, digitalRead(BUILTIN_LED) ^ HIGH);
  }
}

/**
 * setup all the hardware
 * @method setup
 */
void setup() {
  // init led buildin
  led_sign_initialize();

  // init debug port
  APP_DEBUG_INIT();

  // serial setup
  APP_DEBUG_PRINT("\r\nSTARTING NTP SERVER");

  // rtc setup
  if (!DEV_RTC_LOCAL.begin()) {
    APP_DEBUG_PRINT("Couldn't find RTC");
    for (;;)
      ;
  }

  // rtc lost power
  if (DEV_RTC_LOCAL.lostPower()) {
    APP_DEBUG_PRINT("RTC lost power, lets set the time!");
    // following line sets the RTC to the date & time this sketch was compiled
    // DEV_RTC_LOCAL.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // DEV_RTC_LOCAL.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  // init wifi setting
  wifi_ap_sta_initialize();

#ifdef WEBSERVER_DEBUG_ON
  // setup the server
  http_server_initialize();
#endif

#ifdef TELNET_DEBUG_ON
  telnet_debugger_initialize();
#endif

  t_now = millis();
  RTCcounts = micros();
}

void loop() {
  // current millis
  t_now = millis();

  // toggle led sign
  led_sign_toggle();

  // read rtc
  dev_rtc_get_current_datetime();

  ntp_request_processed();

#ifdef WEBSERVER_DEBUG_ON
  // server http client
  HTTP_SERVER.handleClient();
#endif

#ifdef TELNET_DEBUG_ON
  TELNET_DEBUGGER.handle();
#endif
}
