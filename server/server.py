'''
https://gist.github.com/micktwomey/606178
'''

import sys
import multiprocessing
import socket
import time, datetime
from string import Formatter

#db
import sqlite3, json

#gpio
import RPi.GPIO as GPIO

#printer
from escpos import constants, escpos, exceptions, printer
from escpos.printer import File
#import escpos

'''
info beamer port udp
'''
INFO_BEAMER_UDP = {
	'HOST': '127.0.0.1',
	'PORT': 4444
}
INFO_BEAMER_SOCKET_ADDR = (INFO_BEAMER_UDP['HOST'],INFO_BEAMER_UDP['PORT'])
# info beamer udp socket
INFO_BEAMER_SOCKET = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

# global vars
PRINTER_OUTPUT = None

'''
	get last error msg
'''
def GetLastErrorMsg():
	return sys.exc_info()[1]
	
'''
Info beamer udp command
'''
def InfoBeamerUDPCommand(cmdx=None):
	err_val=0
	err_msg=''
	try:
		if cmdx is not None:
			sent = INFO_BEAMER_SOCKET.sendto(cmdx,INFO_BEAMER_SOCKET_ADDR)
			if sent == len(cmdx):
				err_msg = 'OK'
				err_val = 0
			else:
				err_msg = 'Not all message sent'
				err_val = 1
		else:
			err_msg = 'No command'
			err_val = 1
	except:
		pass
		err_msg = GetLastErrorMsg()
		err_val = 1
		
	return (err_val,err_msg)

'''
https://stackoverflow.com/a/42320260
https://stackoverflow.com/questions/538666/python-format-timedelta-to-string
'''
def strfdelta(tdelta, fmt='{D:02}d {H:02}h {M:02}m {S:02}s', inputtype='timedelta'):
	"""Convert a datetime.timedelta object or a regular number to a custom-
	formatted string, just like the stftime() method does for datetime.datetime
	objects.

	The fmt argument allows custom formatting to be specified.  Fields can 
	include seconds, minutes, hours, days, and weeks.  Each field is optional.

	Some examples:
		'{D:02}d {H:02}h {M:02}m {S:02}s' --> '05d 08h 04m 02s' (default)
		'{W}w {D}d {H}:{M:02}:{S:02}'	 --> '4w 5d 8:04:02'
		'{D:2}d {H:2}:{M:02}:{S:02}'	  --> ' 5d  8:04:02'
		'{H}h {S}s'					   --> '72h 800s'

	The inputtype argument allows tdelta to be a regular number instead of the  
	default, which is a datetime.timedelta object.  Valid inputtype strings: 
		's', 'seconds', 
		'm', 'minutes', 
		'h', 'hours', 
		'd', 'days', 
		'w', 'weeks'
	"""

	# Convert tdelta to integer seconds.
	if inputtype == 'timedelta':
		remainder = int(tdelta.total_seconds())
	elif inputtype in ['s', 'seconds']:
		remainder = int(tdelta)
	elif inputtype in ['m', 'minutes']:
		remainder = int(tdelta)*60
	elif inputtype in ['h', 'hours']:
		remainder = int(tdelta)*3600
	elif inputtype in ['d', 'days']:
		remainder = int(tdelta)*86400
	elif inputtype in ['w', 'weeks']:
		remainder = int(tdelta)*604800

	f = Formatter()
	desired_fields = [field_tuple[1] for field_tuple in f.parse(fmt)]
	possible_fields = ('W', 'D', 'H', 'M', 'S')
	constants = {'W': 604800, 'D': 86400, 'H': 3600, 'M': 60, 'S': 1}
	values = {}
	for field in possible_fields:
		if field in desired_fields and field in constants:
			values[field], remainder = divmod(remainder, constants[field])
	return f.format(fmt, **values)
	
'''
read end file
'''
def read_end_datetime_file():
	try:
		try:
			file = open("/mnt/data/endtime.txt","r")
		except:
			pass
			
		try:
			file = open("/etc/countdown/endtime.txt","r")
		except:
			pass
			
		try:
			file = open("endtime.txt","r")
		except:
			pass
			
		strx = file.read()
		file.close()
		strx = strx.rstrip("\r\n")
		return strx
	except:
		pass
		return "2018-8-18 0:0:0"

'''
get day diff time
'''
def get_delta_days_time():
	try:
		d0 = datetime.datetime.strptime(read_end_datetime_file(),"%Y-%m-%d %H:%M:%S")
		d1 = datetime.datetime.now()
		return strfdelta(d0-d1,"{D},{H},{M},{S}")
	except:
		pass
		return "0,0,0,0"

'''
main connection handler
'''
def handle(connection, address):
	import logging
	logging.basicConfig(level=logging.DEBUG)
	logger = logging.getLogger("process-%r" % (address,))
	try:
		logger.debug("Connected %r at %r", connection, address)
		while True:
			'''
			data = connection.recv(1024)
			if data == "":
				logger.debug("Socket closed remotely")
				break
			logger.debug("Received data %r", data)
			'''
			'''
			connection.sendall(data)
			connection.sendall(get_delta_days_time())
			logger.debug("Sent data")
			'''
			
			data = connection.recv(1024)
			if data == "":
				logger.debug("Socket closed remotely")
				break
			logger.debug("Received data %r", data)
			
			try:
				#split the command
				cmd_detil = data.split(',')
				logger.debug("Parsed command = %r", cmd_detil)
				
				cmd_head = cmd_detil[0]
								
				#command request nomor 
				if cmd_head == 'c':
					logger.debug("Request new number for LINE %s", cmd_detil[1])
					
				#command recall
				if cmd_head == 'r':
					logger.debug("Recall for LINE %s", cmd_detil[1])
					
			except:
				pass
				logger.exception("Got error handling the command")
			#time.sleep(0.05)
	except:
		logger.exception("Problem handling request")
	finally:
		logger.debug("Closing socket")
		connection.close()

class Server(object):
	def __init__(self, hostname, port):
		import logging
		self.logger = logging.getLogger("server")
		self.hostname = hostname
		self.port = port

	def start(self):
		self.logger.debug("listening")
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.bind((self.hostname, self.port))
		self.socket.listen(1)

		while True:
			conn, address = self.socket.accept()
			self.logger.debug("Got connection")
			process = multiprocessing.Process(target=handle, args=(conn, address))
			process.daemon = True
			process.start()
			self.logger.debug("Started process %r", process)
		
class GPIO_CLS(object):
	def __init__(self, printer_output=None):
		import logging
		self.logger = logging.getLogger("gpio")
		self.is_printing = False
			
		try:
			GPIO.setwarnings(False)
			GPIO.setmode(GPIO.BCM)
			GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)
			GPIO.add_event_detect(21, GPIO.FALLING, callback=self.gpio_switch_callback, bouncetime=200)
			self.printer_output = printer_output
			
			self.logger.debug("GPIO init OK")
		except:
			pass
			self.logger.exception("GPIO init fail at %s" % (GetLastErrorMsg()))
		
	def cleaning(self):
		try:
			GPIO.cleanup()
			self.logger.debug("GPIO cleanup OK")
		except:
			pass
			self.logger.exception("GPIO cleanup fail at %s" % (GetLastErrorMsg()))
	
	def gpio_switch_callback(self, channel):
		try:
			if not self.is_printing:
				self.is_printing = True
				self.logger.debug("GPIO start printing...")
				
				print("%f : GPIO %s pressed" % (time.time(), channel))
				self.printer_output.print_note(numberx=channel)
				
				self.logger.debug("GPIO printing OK")
				self.is_printing = False
		except:
			pass
			self.is_printing = False
			self.logger.exception("GPIO printing fail at %s" % (GetLastErrorMsg()))
		
class PRINTER_EPSON(object):
	def __init__(self,printer_dev_name="/dev/usb/lp0"):
		import logging
		self.logger = logging.getLogger("printer")
		
		try:
			self.printer_obj = printer.File(printer_dev_name)
			self.logger.debug("Printer init OK")
		except:
			pass
			self.printer_obj = None
			self.logger.exception("Printer init fail at %s" % (GetLastErrorMsg()))
					
	def print_note(self,numberx=0):
		try:
			self.logger.debug("Printer start printing")
			
			if self.printer_obj is not None:
				self.printer_obj.text("HELLO WORLD\n")
				self.printer_obj.cut()
			else:
				self.logger.warning("Printer not opened!!!")
			
			self.logger.debug("Printer end printing")
		except:
			pass
			self.logger.exception("Printer note fail at %s" % (GetLastErrorMsg()))
	
'''
main loop here
'''
def main_loop():
	import logging
	logging.basicConfig(level=logging.DEBUG)
	
	global PRINTER_OUTPUT
	PRINTER_OUTPUT = PRINTER_EPSON()
	gpiox = GPIO_CLS(printer_output=PRINTER_OUTPUT)
	
	server = Server("0.0.0.0", 50000)
	try:
		logging.info("Listening")
		server.start()
	except:
		logging.exception("Unexpected exception")
	finally:
		logging.info("Shutting down")
		for process in multiprocessing.active_children():
			logging.info("Shutting down process %r", process)
			process.terminate()
			process.join()
	
	# cleanup
	gpiox.cleaning()
	logging.info("All done")
	
'''
main program start here
'''
if __name__ == "__main__":
	main_loop()
