/*
sounder untuk suara
*/

//lib
#include <stdio.h>
#include <stdlib.h>

//sound def
#define	SND_FOLDER	"./snd/"
#define SND_PROLOG	"prolog.wav"
#define	SND_EPILOG	"epilog.wav"
#define PLAY_CMD1 	"sox %s%s -t waveaudio 1 -q"	//for cygwin
#define PLAY_CMD2 	"aplay %s%s -q"	//for arch linux

#define	ID_SND_SEBELAS	10
#define	ID_SND_SERATUS	11
#define	ID_SND_SERIBU	12
#define	ID_SND_BELASAN	13
#define	ID_SND_PULUHAN	14
#define	ID_SND_RATUSAN	15
#define	ID_SND_RIBUAN	16

//sound files
char SND_FILES[20][15] = {"1.wav", "2.wav", "3.wav", "4.wav", "5.wav", "6.wav", "7.wav", "8.wav", "9.wav", "10.wav", "11.wav", "100.wav", "1000.wav", "belas.wav", "puluh.wav", "ratus.wav", "ribu.wav"};

/*
 * proto fucntion list
 * */
//~ void announce(unsigned int cso, unsigned int queue_number);

/*
 * parsing nomor ke array suara
 * */
unsigned char *parse_number_to_sound_array(unsigned int anumber, int *result_len)
{
	//var
	int i=0;
	unsigned int numx, numy;
	unsigned char *result = malloc(15 * sizeof(unsigned char));
	
	//decode ribuan
	numx = anumber/1000;
	if(numx>0)
	{
		if(numx==1)	//seribuan
		{
			result[i++] = ID_SND_SERIBU;
		}
		else //>1		
		{
			result[i++] = (unsigned char) numx-1;
			result[i++] = ID_SND_RIBUAN;
		}
	}
	
	//decode ratusan
	numx = (anumber % 1000) / 100;
	if(numx>0)
	{
		if(numx==1)	//seratus
			result[i++] = ID_SND_SERATUS;
		else
		{	
			result[i++] = (unsigned char) numx-1;
			result[i++] = ID_SND_RATUSAN;
		}
	}
	
	//decode puluhan
	numy = anumber % 100;
	numx = numy / 10;
	if(numx>0)
	{
		if(numy==10)	//sepuluh
			result[i++] = (unsigned char) numy-1;
		else if (numy==11) //sebelas
			result[i++] = (unsigned char) numy-1;
		else if((numy>11) && (numy<20)) //12-19
		{
			result[i++] = (unsigned char) (numy % 10)-1;
			result[i++] = ID_SND_BELASAN;
		}
		else //>=20
		{	
			result[i++] = (unsigned char) numx-1;
			result[i++] = ID_SND_PULUHAN;
			
			//decode satuan, jika ada
			if(anumber % 10 > 0)
				result[i++] = (unsigned char) (anumber % 10)-1;
		}
	}
	else //satuan
	{
		//decode satuan, jika ada
		if(anumber % 10 > 0)
			result[i++] = (unsigned char) (anumber % 10)-1;
	}
	
	//realloc hasil
	//~ printf("l0 = %d\n", i);
	result = (unsigned char*) realloc(result, i*sizeof(unsigned char));
	*result_len = i;
	
	//~ printf("l1 = %d\n", i);
	
	//return result
	return result;
}

/*
 * play suara
 * */
void play_sound(char *asound, unsigned char mode_snd) 
{
	char fnx[50];
	
	snprintf(fnx,50,(mode_snd==0) ? PLAY_CMD1:PLAY_CMD2,SND_FOLDER, asound);
	system(fnx);
}

/*
 * play array sound
 * */
void play_sound_array(unsigned char *snd_array, int array_len, unsigned char mode_sound)
{
	int i;
	
	//~ printf("len=%d\n", array_len);
	for(i=0;i<array_len;i++)
	{
		//~ printf("%d\t=> %s\n",i+1, SND_FILES[snd_array[i]]);
		play_sound(SND_FILES[snd_array[i]], mode_sound);
	}
}

/*
 * make annoucnement
 * */
void announce(unsigned int cso, unsigned int queue_number, unsigned char mode_dev)
{
	int lenx;
	unsigned char *sndx=NULL;
	
	//debug	
	//~ printf("cso = %d\tnum = %d\n", cso, queue_number);
	play_sound(SND_PROLOG,mode_dev);
	
	sndx = parse_number_to_sound_array(queue_number, &lenx);
	
	//~ printf("len1 = %d => %s\n", lenx, SND_FILES[sndx[0]]);
	
	play_sound_array(sndx, lenx, mode_dev);
	
	play_sound(SND_EPILOG, mode_dev);
	
	sndx = parse_number_to_sound_array(cso, &lenx);
	
	//~ printf("len2 = %d => %s\n", lenx, SND_FILES[sndx[0]]);
	
	play_sound_array(sndx, lenx, mode_dev);
	
	free(sndx);
}

//main prgoram
int main(int argc, char **argv)
{
	if(argc<4)
		return 1;
		
	unsigned int cso=atoi(argv[2]), numx=atoi(argv[3]);
	unsigned char amode_snd = atoi(argv[1]);
	
	if(cso>0 && numx>0)
		announce(cso, numx, amode_snd);	
	return 0;
}
