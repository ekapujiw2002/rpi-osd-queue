-- at which intervals should the screen switch to the
-- next image?
local INTERVAL = 5

-- enough time to load next image
local SWITCH_DELAY = 1

-- transition time in seconds.
-- set it to 0 switching instantaneously
local SWITCH_TIME = 1.0


-- Media directory. Set to '' to have your
-- images in the current directory.
local MEDIA_DIRECTORY = 'assets'
-- local MEDIA_DIRECTORY = 'media'

--[[
var cs number
--]]
local CS_NUMBER_VALUE = {0,0,0}

----------------------------------------------------------------
assert(SWITCH_TIME + SWITCH_DELAY < INTERVAL,
    "INTERVAL must be longer than SWITCH_DELAY + SWITCHTIME")
	
-- set fast garbage collection, avoiding out of memory
node.set_flag("slow_gc", false)

-- set gl texture accoring to the device height and width
gl.setup(NATIVE_WIDTH, NATIVE_HEIGHT)

-- auto init all resources
local ALL_CONTENTS, ALL_CHILDS = node.make_nested()

-- load all resource automatically
-- make sure the name is all different!!!
util.auto_loader(_G)

-- get the content to show
local function feeder()
    return {"Selamat datang di Alexis", "Rilex and enjoy the show"}
end

--[[
setup running text
--]]
run_text = util.running_text{
    font = consola;
    size = 50;
    speed = 100;
    color = {0,0,0,1};
    generator = util.generator(feeder)
}

-- local background = resource.load_image("bg.jpg")

--[[
some pixel colored resource
--]]
local white = resource.create_colored_texture(1,1,1,0.5)
local blue = resource.create_colored_texture(0,0,1,0.8)
local black = resource.create_colored_texture(0,0,0,0.8)

--[[
split string to table
--]]
local function split(str, sep)
   local result = {}
   local regex = ("([^%s]+)"):format(sep)
   for each in str:gmatch(regex) do
      table.insert(result, each)
   end
   return result
end

--[[
sorting object based on character
--]]
local function alphanumsort(o)
    local function padnum(d) return ("%03d%s"):format(#d, d) end
    table.sort(o, function(a,b)
        return tostring(a):gsub("%d+",padnum) < tostring(b):gsub("%d+",padnum)
    end)
    return o
end

--[[
generate picture playlist
--]]
local pictures = util.generator(function()
    local files = {}
    for name, _ in pairs(ALL_CONTENTS[MEDIA_DIRECTORY]) do
        if name:match(".*jpg") or name:match(".*png") then
            files[#files+1] = name
        end
    end
    return alphanumsort(files) -- sort files by filename
end)



local current_image = resource.create_colored_texture(0,0,0,0)
local fade_start = 0

--[[
get next image from picture playlist
--]]
local function next_image()
    local next_image_name = pictures.next()
    print("Now loading " .. next_image_name)
    last_image = current_image
    current_image = resource.load_image(next_image_name)
    fade_start = sys.now()
end

--[[
do slideshow
--]]
local function slideshow()
	local delta = sys.now() - fade_start - SWITCH_DELAY
    if last_image and delta < 0 then
        -- util.draw_correct(last_image, 0, 0, WIDTH, HEIGHT)
		last_image:draw(0,0,WIDTH, HEIGHT)
    elseif last_image and delta < SWITCH_TIME then
        local progress = delta / SWITCH_TIME
        -- util.draw_correct(last_image, 0, 0, WIDTH, HEIGHT, 1 - progress)
        -- util.draw_correct(current_image, 0, 0, WIDTH, HEIGHT, progress)
		
		last_image:draw(0, 0, WIDTH, HEIGHT, 1 - progress)
		current_image:draw(0, 0, WIDTH, HEIGHT, progress)
    else
        if last_image then
            last_image:dispose()
            last_image = nil
        end
        -- util.draw_correct(current_image, 0, 0, WIDTH, HEIGHT)
		current_image:draw(0,0,WIDTH, HEIGHT)
    end
end

--[[
run the slideshow
--]]
local function slideshow_running()
	local delta = sys.now() - fade_start - SWITCH_DELAY
    if last_image and delta < 0 then
        -- util.draw_correct(last_image, 0, 0, WIDTH, HEIGHT)
		last_image:draw(0,0,WIDTH, HEIGHT)
    elseif last_image and delta < SWITCH_TIME then
        local progress = delta / SWITCH_TIME
        -- util.draw_correct(last_image, 0, 0, WIDTH, HEIGHT, 1 - progress)
        -- util.draw_correct(current_image, 0, 0, WIDTH, HEIGHT, progress)
		
		last_image:draw(0, 0, WIDTH, HEIGHT, 1 - progress)
		current_image:draw(0, 0, WIDTH, HEIGHT, progress)
    else
        if last_image then
            last_image:dispose()
            last_image = nil
        end
        -- util.draw_correct(current_image, 0, 0, WIDTH, HEIGHT)
		current_image:draw(0,0,WIDTH, HEIGHT)
    end
end

--[[
show title and info device
--]]
local function show_title_and_info()
	local name = "MASJID AL KAROMAH"
	local addr = "JL. MAWAR 15, PALEMBANG, SUMSEL"
	
	white:draw(0,0,WIDTH,150)
	consola:write((WIDTH-consola:width(name, 50))/2,0,name,50,0,0,1,1)
	consola:write((WIDTH-consola:width(addr, 50))/2,50,addr,50,0,0,1,1)
end	

--[[
show current datetime
--]]
local function show_current_datetime()
	local tnow = os.date("%H:%M:%S %d-%m-%Y",os.time())
	consola:write((WIDTH-consola:width(tnow, 40))/2,100,tnow,40,1,0,0,1)
end

--[[
show cs number
--]]
local function show_cs_number()
	black:draw(0,HEIGHT-365,WIDTH,HEIGHT-65)
	lcd_normal:write(100,HEIGHT-350,"CS1",150,0,1,0,1)
	lcd_normal:write(WIDTH/3+100,HEIGHT-350,"CS2",150,0,1,0,1)
	lcd_normal:write(WIDTH*2/3+100,HEIGHT-350,"CS3",150,0,1,0,1)
	
	lcd_normal:write(100,HEIGHT-200,CS_NUMBER_VALUE[1],130,1,0,0,1)
	lcd_normal:write(WIDTH/3+100,HEIGHT-200,CS_NUMBER_VALUE[2],130,1,0,0,1)
	lcd_normal:write(WIDTH*2/3+100,HEIGHT-200,CS_NUMBER_VALUE[3],130,1,0,0,1)
end

--[[
show top text
--]]
local function show_top_text()
	local name = "NAMA"
	local addr = "WHERE"
	
	white:draw(0,0,WIDTH,150)
	consola:write((WIDTH-consola:width(name, 50))/2,0,name,50,0,0,1,1)
	consola:write((WIDTH-consola:width(addr, 50))/2,50,addr,50,0,0,1,1)
	
	local tnow = os.date("%H:%M:%S %d/%m/%Y",os.time())
	lcd_normal:write((WIDTH-lcd_normal:width(tnow, 40))/2,105,tnow,40,1,0,0,1)
end

--[[
show bottom text
--]]
local function show_bottom_text()
	white:draw(0,HEIGHT-60,WIDTH,HEIGHT)
	run_text:draw(HEIGHT-55)
end

--[[
some debug info
--]]
--[[
pp(run_text)
pp(pictures)
pp(consola)
pp(os.time())
--]]

--[[
local video_file = resource.open_file("assets/crysis2.mp4")
local video_obj = resource.load_video{
    file = video_file;
    looped = true;
}
pp(video_obj)
--]]

--[[
node event fired when item move
--]]
node.event("content_remove", function(filename)
    pictures:remove(filename)
end)

--[[
node event udp
send the command using :
echo -ne "countdown/set_video:next" > /dev/udp/localhost/4444
echo -ne "countdown/timer/1800:start" > /dev/udp/localhost/4444
--]]
node.event("data", function(data, suffix)
	print("UDP data = " .. data .. " -- " .. suffix)
end)

--[[
command mapper for udp packet
--]]
util.data_mapper{

--[[
count cs : osd/count:
--]]
    ["count"] = function(count_data)
        print("UDP mapper [count] = " .. count_data)
		local valx = split(count_data,",")
		pp(valx)
		
		for idx,itemx in ipairs(valx) do
			CS_NUMBER_VALUE[idx] = tonumber(itemx)
		end
		pp(CS_NUMBER_VALUE)
		
    end -- ,
	
--[[
timer/[duration in seconds]:set,reset,start,stop,display,hide
--]]	
--[[
	["timer/([0-9]+)"] = function(timer_data, timer_cmd)
		local secx = tonumber(timer_data)
		print("UDP mapper [timer] = " .. timer_cmd .. " " .. secx)
		
		if timer_cmd:match("set") or timer_cmd:match("reset") then
			timer_countdown_init(secx)
		end
		
		if timer_cmd:match("start") then
			timer_countdown_start()
		end
		
		if timer_cmd:match("stop") then
			timer_countdown_stop()
		end
		
		if timer_cmd:match("show") then
			timer_countdown_displayed = 1
		end
		
		if timer_cmd:match("hide") then
			timer_countdown_displayed = 0
		end
	end
	--]]
}

--[[
main node render function
--]]
function node.render()	
    -- clear the screen
	gl.clear(0,0,0,1)
	--gl.scale(0.5,0.5)
	
    -- slideshow
	slideshow_running()
	
	-- cs text
	show_cs_number()
	
	-- top text
	show_top_text()
	
	-- setup bottom running text
	show_bottom_text()
end

--[[
set the interval of next image
--]]
util.set_interval(INTERVAL, next_image)

