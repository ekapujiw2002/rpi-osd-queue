'''
web server antrian raspi
twisted based
'''

__author__ = "EX4"
__application_name__ = "SERVER ANTRIAN RPI"
__version__ = "20092018-150755"

'''
main library
'''
import os
import sys

# signal osses
import signal

# argument parser
from argparse import ArgumentParser

# lib for config file parser

# enumerate

# json
import json

# datetime
import datetime

# twisted
from twisted.internet import reactor
# from twisted.internet.defer import inlineCallbacks, returnValue
from twisted.python import log
# from twisted.internet.protocol import connectionDone
# from twisted.python.logfile import DailyLogFile
# from twisted.enterprise import adbapi

# for web server part
from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET
from twisted.web.static import File

# autobahn lib
# from autobahn.twisted.websocket import WebSocketServerFactory, \
#     WebSocketServerProtocol, \
#     listenWS
from autobahn.twisted.resource import WebSocketResource

# printer
from escpos import constants, escpos, exceptions, printer
from escpos.printer import File as escposFile

# SSE
from library.SSEResource import SseResource
from library.SystemUtil import SystemUtil
from library.BroadcastServer import BroadcastServerFactory, BroadcastServerProtocol
from library.UDPClient import EchoClientDatagramProtocol
from library.UploadWebHandler import UploadWebHandler

# configuration file
CONFIG_FILE = "config.ini"

'''
web server setting
'''
WEB_SERVER_SETTING = {
    'ROOT_PATH': './www/',
    'HTTP': 8090,
    'DEBUG_MODE': True,
    'UPLOAD_DIR': './www/upload/'
}

'''
os server setting
'''
OSD_SERVER_SETTING = {
    'PORT': 4445
}

'''
Server sent events global object
'''
SSE_OBJ = SseResource()

'''
osd udp object and protocol
'''
OSD_PROTOCOL_OBJ = EchoClientDatagramProtocol()

'''
printing struk class
'''


class PRINTER_EPSON(object):
    def __init__(self, printer_dev_name="/dev/usb/lp0"):
        import logging
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger("printer")

        try:
            self.printer_obj = printer.File(printer_dev_name)
            self.logger.debug("Printer init OK")
        except:
            pass
            self.printer_obj = None
            self.logger.exception("Printer init fail at %s" % (SystemUtil.GetLastErrorMsg()))

    def print_note(self, numberx=0):
        try:
            self.logger.debug("Printer start printing")

            if self.printer_obj is not None:
                try:
                    self.printer_obj.set(align='center', text_type='b', height=2, width=2)
                except:
                    pass
                    self.logger.exception("Printer set custom text option fail at %s" % (SystemUtil.GetLastErrorMsg()))

                self.printer_obj.textln("\nSELAMAT DATANG\n")

                try:
                    self.printer_obj.set(align='center', text_type='b', height=6, width=3)
                except:
                    pass
                    self.logger.exception("Printer set custom text option fail at %s" % (SystemUtil.GetLastErrorMsg()))

                self.printer_obj.textln(str(numberx))

                try:
                    self.printer_obj.set(align='center', text_type='normal', height=2, width=2)
                except:
                    pass
                    self.logger.exception("Printer set custom text option fail at %s" % (SystemUtil.GetLastErrorMsg()))

                self.printer_obj.textln(str(datetime.datetime.strftime(datetime.datetime.now(), "%d/%m/%Y %H:%M:%S")))
                self.printer_obj.textln("Terimakasih Anda telah tertib\n")

                self.printer_obj.cut()
            else:
                self.logger.warning("Printer not opened!!!")

            self.logger.debug("Printer end printing")

            return True
        except:
            pass
            self.logger.exception("Printer note fail at %s" % (SystemUtil.GetLastErrorMsg()))
            return False


'''
osd control from web
'''


class HTTPOSDServerAPI(Resource):
    isLeaf = True

    def __init__(self):
        Resource.__init__(self)

    def process_data(self, request):
        try:
            retx = {
                'err': 0,
                'response': {
                    'code': 0,
                    'message': ''
                }
            }
            paramx = request.postpath
            log.msg(paramx)
            if len(paramx) > 0 and len(paramx[0]) > 0:
                cmd = paramx[0].decode()
                # log.msg(cmd)

                if cmd == "call":
                    id_cso = paramx[1]

                    return NOT_DONE_YET
                elif cmd == "recall":
                    id_cso = paramx[1]

                    return NOT_DONE_YET
                # elif cmd == "login":
                #     user_id = paramx[1].decode()
                #     user_pass = paramx[2].decode()
                #     user_unit_id = paramx[3].decode()
                #     # PDAMProcessor().get_login_info_from_web(user_id, user_pass, user_unit_id, request)
                #     return NOT_DONE_YET
                elif cmd == "test":
                    payload = paramx[1]
                    reactor.callFromThread(EchoClientDatagramProtocol.sendDatagramPayload, OSD_PROTOCOL_OBJ, payload)
                    # return NOT_DONE_YET
                    retx['err'] = 0
                    retx['response']['code'] = 0
                    retx['response']['message'] = 'OK'
                else:
                    # return "{\"err\":1,\"response\":{\"message\":\"PARAMETER INVALID\"}}".encode()
                    retx['err'] = 1
                    retx['response']['code'] = 2
                    retx['response']['message'] = 'Perintah invalid'
            else:
                # return "{\"err\":1,\"response\":{\"message\":\"PARAMETER INVALID\"}}".encode()
                retx['err'] = 1
                retx['response']['code'] = 1
                retx['response']['message'] = 'Parameter invalid'
        except Exception as err:
            pass
            retx['err'] = 1
            retx['response']['code'] = 255
            retx['response']['message'] = 'Error tidak diketahui'

        log.msg(retx)
        request.write(json.dumps(retx).encode())
        request.finish()

    def render_GET(self, request):
        try:
            self.process_data(request)
            return NOT_DONE_YET
        except Exception as err:
            pass
            return "{\"err\":1,\"response\":{\"message\":\"Error tidak diketahui\"}}".encode()

    def render_POST(self, request):
        try:
            self.process_data(request)
            return NOT_DONE_YET
        except Exception as err:
            pass
            return "{\"err\":1,\"response\":{\"message\":\"Error tidak diketahui\"}}".encode()

    def render_PUT(self, request):
        try:
            self.process_data(request)
            return NOT_DONE_YET
        except Exception as err:
            pass
            return "{\"err\":1,\"response\":{\"message\":\"Error tidak diketahui\"}}".encode()


'''
Main parameter handler program
'''


def __handle_options():
    parser = ArgumentParser()
    '''
    parser.add_argument("-i", "--id", dest="api_user_id", required=True, type=int,
                                      help="[REQUIRED] API User Id.", metavar="API_USER_ID")
    parser.add_argument("-t", "--token", dest="api_token", required=True,
                                      help="[REQUIRED] API Auth Token.", metavar="API_AUTH_TOKEN")
    parser.add_argument("-u", "--url", dest="api_url", default="wss://api.rt.speechmatics.io:9000",
                                      help="URL of the APIs WebSocket")
    parser.add_argument("-l", "--lang", dest="lang", required=True,
                    help="[REQUIRED] Language to use (e.g., en-US). See https://www.speechmatics.com/support for a list of currently supported languages.", metavar="LANG")
    '''
    parser.add_argument("-c", "--config", dest="cfg_filename", default="config.ini",
                        help="Switching server configuration file")

    args = parser.parse_args()
    return args


'''
capture signal interrupt
'''


def __setupSignalHandler(audioSource):
    """Setup signal handler for the initial Ctrl+C to only stop the input. The whole processing is stopped with another Ctrl+C"""

    def sigint_handler(received_signal, frame):
        if reactor.running:
            reactor.stop()

        # force to exit
        # https://stackoverflow.com/questions/73663/terminating-a-python-script
        os._exit(1)

    # register the signl handler
    signal.signal(signal.SIGINT, sigint_handler)


'''
MAIN PROGRAM
'''


def main_app_loop():
    try:
        # start logging
        log.startLogging(sys.stdout)

        # read program options
        options = __handle_options()

        # read configuration
        global CONFIG_FILE
        CONFIG_FILE = options.cfg_filename

        # get server setting
        global WEB_SERVER_SETTING
        WEB_SERVER_SETTING['HTTP'] = int(SystemUtil.read_config_file(CONFIG_FILE, 'web-server', 'http'))
        WEB_SERVER_SETTING['ROOT_PATH'] = SystemUtil.read_config_file(CONFIG_FILE, 'web-server', 'doc')
        WEB_SERVER_SETTING['UPLOAD_DIR'] = SystemUtil.read_config_file(CONFIG_FILE, 'web-server', 'upload_dir')
        log.msg(WEB_SERVER_SETTING)

        # first message
        log.msg(__application_name__ + " - " + __version__)
        log.msg("Serving configuration from %s" % (CONFIG_FILE,))

        # the server for webserver and websocket
        # root web dir
        root_webdir = File(WEB_SERVER_SETTING['ROOT_PATH'])

        # ordinary websocket
        WebsocketServerFactory = BroadcastServerFactory
        websocket_server_url = ("ws://127.0.0.1:{}".format(WEB_SERVER_SETTING['HTTP']))
        log.msg(websocket_server_url)
        websocket_server_factory = WebsocketServerFactory(websocket_server_url)
        websocket_server_factory.protocol = BroadcastServerProtocol
        websocket_server_resource = WebSocketResource(websocket_server_factory)

        # and our WebSocket server under "/ws" (note that Twisted uses
        # bytes for URIs)
        root_webdir.putChild(b"ws", websocket_server_resource)

        # report child
        # root_webdir.putChild(b"report", File(SWITCHING_SERVER_SETTING['REPORT_DIR']))

        # web api child
        root_webdir.putChild(b"api", HTTPOSDServerAPI())

        # sse
        root_webdir.putChild(b"sse", SSE_OBJ)

        # uploader
        root_webdir.putChild(b"upl", UploadWebHandler(upload_dir=WEB_SERVER_SETTING['UPLOAD_DIR']))

        # static website
        website_static = Site(root_webdir)

        # spawn http server
        reactor.listenTCP(WEB_SERVER_SETTING['HTTP'], website_static)

        # spawn osd udp server
        reactor.listenUDP(int(SystemUtil.read_config_file(CONFIG_FILE, 'osd-server', 'port')), OSD_PROTOCOL_OBJ)

        # start the main twisted reactor
        log.msg("Start the main thread")

        # run the main reactor
        reactor.run()
    except Exception as err:
        pass
        log.err(err)


'''
main program
'''
if __name__ == "__main__":
    main_app_loop()
