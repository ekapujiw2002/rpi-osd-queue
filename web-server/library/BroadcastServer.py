'''
https://github.com/crossbario/autobahn-python/blob/master/examples/twisted/websocket/broadcast/server.py
'''
import json

from autobahn.twisted import WebSocketServerProtocol, WebSocketServerFactory
from twisted.internet import reactor

# from library import SystemUtil


class BroadcastServerProtocol(WebSocketServerProtocol):

    def onOpen(self):
        """
        when connection opened
        """
        self.factory.register(self)
        # self.sendMessage(("Welcome to %s - %s" % (__application_name__, __version__)).encode("utf8"), isBinary=False)
        # self.factory.broadcast(("Welcome to %s - %s" % (__application_name__, __version__)))

    def onMessage(self, payload, isBinary):
        """
        got message from client
        :param payload: the data
        :param isBinary: binary or text?
        """
        if not isBinary:
            msg = "{} from {}".format(payload.decode('utf8'), self.peer)
            self.factory.broadcast(msg)

    def connectionLost(self, reason):
        """
        when client disconnected
        :param reason: the reason of disconnection
        """
        WebSocketServerProtocol.connectionLost(self, reason)
        self.factory.unregister(self)


'''
https://stackoverflow.com/questions/28799754/sendmessage-from-outside-in-autobahn-running-in-separate-thread
'''


class BroadcastServerFactory(WebSocketServerFactory):
    """
    Simple broadcast server broadcasting any message it receives to all
    currently connected clients.
    """

    clients = []

    def __init__(self, url):
        """
        initialize the class
        :param url: the url of the server
        """
        WebSocketServerFactory.__init__(self, url)
        # self.clients = []
        self.tickcount = 0
        self.tick()

    def tick(self):
        """
        just some simple timed callback routine
        """
        # self.tickcount += 1
        # self.broadcast("tick %d from server" % self.tickcount)

        # cpu_ram = get_cpu_ram_usage()
        '''
        self.broadcast("{ "
                       "\"timestamp\": %f,"
                       "\"error\": 0,"
                       "\"data\": \"{\\\"cpu\\\":%.0f, \\\"ram\\\":%f}\""
                       "}" % (time.time(), cpu_ram['cpu'], cpu_ram['ram']))
        '''
        # self.broadcast(SystemUtil.get_all_status(in_json=True, cfg_file=CONFIG_FILE))
        # SSE_OBJ.write([SystemUtil.get_all_status(in_json=True, cfg_file=CONFIG_FILE)], "server-log")
        reactor.callLater(30, self.tick)

    def register(self, client):
        """
        register new client
        :param client: client object
        """
        if client not in self.clients:
            print("Registered client {}".format(client.peer))
            self.clients.append(client)
            # welcome_msg = "{\"timestamp\":%f,\"error\":0,\"data\":\"%s\"}" % (
            #     time.time(), ("Welcome to %s - %s" % (__application_name__, __version__)))
            # client.sendMessage(welcome_msg.encode("utf8"), isBinary=False)

    def unregister(self, client):
        """
        unregister client from pool
        :param client: client object
        """
        if client in self.clients:
            print("Unregistered client {}".format(client.peer))
            self.clients.remove(client)

    def broadcast(self, msg):
        """
        broadcast message to all connected client. this is class method.don't run it directly.
        :param msg: the data
        """
        print("Broadcasting message '{}' ..".format(msg))
        for c in self.clients:
            c.sendMessage(msg.encode('utf8'))
            print("Message sent to {}".format(c.peer))

    @classmethod
    def broadcast_message(cls, data):
        """
        broadcast message to all client. this is supposed to be called directly from another thread.
        :param data:
        """
        # payload = json.dumps(data, ensure_ascii = False).encode('utf8')
        payload = json.dumps(data)
        # print(payload)
        reactor.callFromThread(cls.broadcast, cls, payload)
        # for c in set(cls.clients):
        '''
        for c in cls.clients:
            reactor.callFromThread(cls.sendMessage, c, payload)
            print("message sent to {}".format(c.peer))
        '''


class BroadcastPreparedServerFactory(BroadcastServerFactory):
    """
    Functionally same as above, but optimized broadcast using
    prepareMessage and sendPreparedMessage.
    """

    def broadcast(self, msg):
        print("Broadcasting prepared message '{}' ..".format(msg))
        preparedMsg = self.prepareMessage(msg)
        for c in self.clients:
            c.sendPreparedMessage(preparedMsg)
            print("Brepared message sent to {}".format(c.peer))