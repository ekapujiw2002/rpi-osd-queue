'''
UDP client protocol
'''
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.python import log


# from twisted.internet.task import LoopingCall


class EchoClientDatagramProtocol(DatagramProtocol):

    def __init__(self, host='127.0.0.1', port=4444):
        self.host = host
        self.port = port

    def startProtocol(self):
        self.transport.connect(self.host, self.port)
        log.msg('UDP : Connected to = {}:{}'.format(self.host, self.port))

    # Possibly invoked if there is no server listening on the
    # address to which we are sending.
    def connectionRefused(self):
        log.msg("UDP : No one listening")

    '''
    def sendDatagram(self):
        if len(self.strings):
            datagram = self.strings.pop(0)
            self.transport.write(datagram)
        else:
            reactor.stop()
    '''

    def datagramReceived(self, datagram, host):
        log.msg('UDP : Datagram received: {}'.format(repr(datagram)))
        # self.sendDatagram()
        # self.sendDatagramPayload(b"Hi...")

    def sendDatagramPayload(self, msg):
        if type(msg) is str:
            self.transport.write(msg.encode())
        else:
            self.transport.write(msg)

    '''
    def sendDatagramPayloadTo(self, host='127.0.0.1', port=20000, msg=None):
        print(repr(self.transport))
        self.transport.write(msg,(host,port))

    #https://twistedmatrix.com/documents/current/core/howto/threading.html
    @classmethod
    def sendDatagramSafe(cls, msg):
        reactor.callFromThread(cls.sendDatagramPayloadTo, cls, msg.encode())
    '''


'''
test code
'''
# def main():
#     protocol = EchoClientDatagramProtocol()
#     t = reactor.listenUDP(44444, protocol)
#
#     # OK
#     # reactor.callFromThread(EchoClientDatagramProtocol.sendDatagramPayload, protocol, "Hi, dude. This is from thread".encode())
#
#     # OK
#     # reactor.callLater(2, EchoClientDatagramProtocol.sendDatagramPayload, protocol, "Hi, dude....".encode())
#
#     # lc = LoopingCall(EchoClientDatagramProtocol.sendDatagramPayload, protocol, "Hi, dude....".encode())
#     # lc.start(1)
#     reactor.run()
#
#
# if __name__ == '__main__':
#     main()
