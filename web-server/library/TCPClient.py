from twisted.internet.protocol import Protocol, ClientFactory
from twisted.python import log

'''
TCP base client protocol for receiving data
'''


class TCPBaseClientProtocol(Protocol):
    def dataReceived(self, data):
        if self.factory.log_on:
            log.msg('Data received {}'.format(data))
        self.transport.loseConnection()

    def connectionMade(self):
        data = self.factory.data
        if isinstance(data, str):
            self.transport.write(data.encode())
        else:
            self.transport.write(data)
            if self.factory.log_on:
                log.msg('Data sent {}'.format(data))

    def connectionLost(self, reason):
        if self.factory.log_on:
            log.msg('Lost connection because {}'.format(reason))


'''
TCP client factory with protocol
'''


class TCPBaseClientFactory(ClientFactory):
    def __init__(self, data_to_server=None, log_on=True):
        self.data = data_to_server
        self.log_on = log_on

    def startedConnecting(self, connector):
        if self.log_on:
            log.msg('Start connection to {}:{}'.format(connector.host, connector.port))
        # log.msg(vars(connector))

    def buildProtocol(self, addr):
        # log.msg(vars(addr))
        if self.log_on:
            log.msg('Connected to {}:{}'.format(addr.host, addr.port))
        p = TCPBaseClientProtocol()
        p.factory = self
        return p

    def clientConnectionLost(self, connector, reason):
        if self.log_on:
            log.msg('Lost connection. Reason: {}'.format(reason))
        # reactor.stop()

    def clientConnectionFailed(self, connector, reason):
        if self.log_on:
            log.msg('Lost failed. Reason: {}'.format(reason))
        # reactor.stop()
