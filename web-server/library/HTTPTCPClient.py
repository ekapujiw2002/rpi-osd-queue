from .TCPClient import TCPBaseClientFactory, TCPBaseClientProtocol
from twisted.python import log

'''
HTTP TCP client protocol for receiving data
'''


class HTTPTCPClientProtocol(TCPBaseClientProtocol):
    def __init__(self):
        super().__init__()

    def dataReceived(self, data):
        super().dataReceived(data)
        # add callback for data processing
        if self.factory.callback_ok is not None and self.factory.http_request is not None:
            self.factory.callback_ok(data, self.factory.http_request)


'''
HTTP TCP client factory with HTTPTCPClientProtocol
'''


class HTTPTCPClientFactory(TCPBaseClientFactory):
    def __init__(self, data_to_server=None, callback_on_success=None, callback_on_error=None, http_request=None,
                 log_on=True):
        super().__init__(data_to_server=data_to_server, log_on=log_on)
        self.callback_ok = callback_on_success
        self.callback_error = callback_on_error
        self.http_request = http_request

    def buildProtocol(self, addr):
        log.msg('Connected to {}:{}'.format(addr.host, addr.port))
        p = HTTPTCPClientProtocol()
        p.factory = self
        return p

    def clientConnectionFailed(self, connector, reason):
        if self.log_on:
            log.msg('Lost failed. Reason: {}'.format(reason))
        # add callback for data processing
        if self.callback_error is not None and self.http_request is not None:
            self.callback_error(reason, self.http_request)
            # self.http_request.finish()
        # reactor.stop()
