'''
This class handle upload from web request
'''
import cgi
import json
import os
from twisted.web.resource import Resource


class UploadWebHandler(Resource):
    isLeaf = True

    def __init__(self, upload_dir=".", upload_form_field_name="file_upload"):
        Resource.__init__(self)
        self.upload_dir = upload_dir
        self.upload_form_field_name = upload_form_field_name

    def render_POST(self, request):
        try:
            # log.msg(request.args[b'namax'][0])
            # log.msg(request.getAllHeaders())

            # get parameter for cgi parser
            method = request.method.decode('utf-8').upper()
            content_type = request.getHeader('content-type')
            content_length = request.getHeader('content-length')

            # feed to cgi parser
            img = cgi.FieldStorage(
                fp=request.content,
                headers={
                    'content-type': content_type,
                    'content-length': content_length
                },
                environ={
                    'REQUEST_METHOD': method,
                    'CONTENT_TYPE': content_type,
                }
            )

            # save to local file
            uploaded_file_path = os.path.join(self.upload_dir,
                                              os.path.basename(img[self.upload_form_field_name].filename))
            with open(uploaded_file_path, 'wb') as fout:
                # fout.write(request.content.read())
                # fout.close()
                fout.write(img[self.upload_form_field_name].file.read())

            return json.dumps({
                'err': 0,
                'message': 'OK'
            }).encode()
        except Exception as err:
            pass
            return json.dumps({
                'err': 1,
                'message': err.message
            }).encode()
