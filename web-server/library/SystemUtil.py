import sys
import configparser
import datetime
import json

import os
import psutil


class SystemUtil:
    '''
        get last error msg
    '''

    @staticmethod
    def GetLastErrorMsg():
        return sys.exc_info()[1]

    @staticmethod
    def generate_unique_id():
        """
        Generate unique id of timestamp upto milliseconds in hex format
        :return:
        """
        return "%x" % (int((datetime.datetime.timestamp(datetime.datetime.now())) * 1000),)

    @staticmethod
    def read_config_file(cfg_filename="config.ini", section_name=None, option_name=None):
        """

        :param cfg_filename:
        :param section_name:
        :param option_name:
        :return:
        """
        try:
            cfgx = configparser.ConfigParser()
            cfgx.read(cfg_filename)
            return cfgx[section_name][option_name]
        except Exception as err:
            pass
            return None

    @staticmethod
    # def get_free_disk_space(path="/mnt/data/pdam-switching/server/logs"):
    def get_free_disk_space(path="logs"):
        """
        Calculate free disk space in MB and Percent
        :param path:
        :return:
        """
        try:
            diskx = psutil.disk_usage(path)
            return {
                'total_mb': round(diskx.total / (1024 * 1024), 3),
                'free_mb': round(diskx.free / (1024 * 1024), 3),
                'free_percent': round(100.0 - diskx.percent, 2)
            }
        except Exception as err:
            pass
            return None

    @staticmethod
    def get_cpu_temperature():
        try:
            res = os.popen('/opt/vc/bin/vcgencmd measure_temp').readline()
            res = res.replace('temp=', '').replace('\'C', '').strip()
            return float(res)
        except Exception as err:
            pass
            return -1.0

    @staticmethod
    def get_all_status(in_json=False, cfg_file="config.ini"):
        try:
            print("Reading status from %s" % (cfg_file,))
            stat_system = SystemUtil.get_cpu_ram_usage()
            stat_system['temperature'] = SystemUtil.get_cpu_temperature()
            resx = {
                'timestamp': datetime.time.time(),
                'tunnel': SystemUtil.get_tunnel_status(cfg_file),
                'storage': SystemUtil.get_free_disk_space(os.path.split(cfg_file)[0]),
                # 'cpu_temp': SystemUtil.get_cpu_temperature(),
                'system': stat_system
            }

            if in_json:
                resx = json.dumps(resx)

            return resx
        except Exception as err:
            pass
            return None

    @staticmethod
    def get_cpu_ram_usage():
        """
        Get cpu and ram usage for this program
        :return:
        """
        try:
            pid = os.getpid()
            py = psutil.Process(pid)
            memory_use = py.memory_info()[0] / 2. ** 20  # memory use in MB
            return {
                'cpu': py.cpu_percent(),
                'ram': memory_use
            }
        except Exception as err:
            pass
            return {'cpu': -1, 'ram': -1}
